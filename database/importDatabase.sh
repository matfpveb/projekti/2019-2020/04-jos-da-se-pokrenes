mongoimport --db just-move-it --collection addresses --file addresses.json --jsonArray
mongoimport --db just-move-it --collection gyms --file gyms.json --jsonArray
mongoimport --db just-move-it --collection statuses --file statuses.json --jsonArray
mongoimport --db just-move-it --collection users --file trainees.json --jsonArray
mongoimport --db just-move-it --collection users --file trainers.json --jsonArray
mongoimport --db just-move-it --collection trainings --file trainings.json --jsonArray