import { Component, OnInit, ɵConsole, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { ProfileComponent } from '../profile/profile.component';
import { EventEmitter } from 'protractor';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile-update',
  templateUrl: './profile-update.component.html',
  styleUrls: ['./profile-update.component.css']
})
export class ProfileUpdateComponent implements OnInit, OnDestroy {

  hideConf = true;
  hide1 = true;
  hide2 = true;

  selectedFile: File = null;


  public user: User;
  private users: User[];

  public checkForm: FormGroup;
  public checkForm2: FormGroup;


  public firstNameChecked: string = "";
  public lastNameChecked: string = "";
  public emailChecked: string = "";
  public birthdayChecked: boolean = false;

  public usernameChecked: string = "";
  public passwordChecked: string = "";
  public passwordChecked2: string = "";

  public genderCheckedMale: boolean = false;
  public genderCheckedFemale: boolean = false;

  public updateConf: boolean = false;


  //  private userSub: Subscription = null;
  private activeSub: Subscription[] = [];


  constructor(private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router, public http: HttpClient) {

    this.user = userService.currentUser;
    this.checkForm = formBuilder.group({
      changeFirstName: ['', [Validators.required, Validators.pattern('[A-Z][a-z]+')]],
      changeLastName: ['', [Validators.required, Validators.pattern('([A-Z][a-z]+)+')]],
      changeEmail: ['', [Validators.required, Validators.email]],
      changeUsername: ['', [Validators.required, Validators.pattern('[_a-zA-Z0-9]+')]],
      changePassword: ['', [Validators.required, Validators.pattern('[a-zA-Z]+[0-9]+')]],
      changePassword2: ['', [Validators.required, Validators.pattern('[a-zA-Z]+[0-9]+')]],
      changeBirthday: new Date(),

    })
    this.checkForm2 = formBuilder.group({
      usernameConf: ['', [Validators.required, Validators.pattern("[a-zA-Z]+[0-9]*")]],
      passwordConf: ['', [Validators.required, Validators.pattern("[a-zA-Z]+[0-9]+")]]

    })
  }

  ngOnInit(): void {
  }

  public get changeFirstName() {
    return this.checkForm.get('changeFirstName');
  }

  public get changeLastName() {
    return this.checkForm.get('changeLastName');
  }

  public get changeEmail() {
    return this.checkForm.get('changeEmail');
  }

  public get changeUsername() {
    return this.checkForm.get('changeUsername');
  }

  public get changePassword() {
    return this.checkForm.get('changePassword');
  }

  public get changePassword2() {
    return this.checkForm.get('changePassword2');
  }

  public get changeBirthday() {
    return this.checkForm.get('changeBirthday');

  }

  public get usernameConf() {
    return this.checkForm2.get('usernameConf');
  }

  public get passwordConf() {
    return this.checkForm2.get('passwordConf');
  }

  public submitForm(data): void {
    console.log(data);
    if (this.acceptChanges() === true) {
      this.updateConf = true;
    }
  }

  public submitForm2(data): void {
    console.log(data);
    if (this.agreeToUpd(data.usernameConf, data.passwordConf) === false) {
      window.alert("Niste uneli tačne podatke.")
      return;
    }

    this.newValues();
    this.updateConf = false;
    this.checkForm.reset();
    this.router.navigate(['/profile', this.user.username]);
    return;
  }

  private acceptChanges(): boolean {
    if (
      (this.firstNameChecked !== "" && !this.changeFirstName.valid) ||
      (this.lastNameChecked !== "" && !this.changeLastName.valid) ||
      (this.birthdayChecked && !this.changeBirthday.valid) ||
      (this.usernameChecked !== "" && !this.changeUsername.valid) ||
      (this.passwordChecked !== "" && !this.changePassword.valid) ||
      (this.passwordChecked2 !== "" && !this.changePassword2.valid) ||
      (this.emailChecked !== "" && !this.changeEmail.valid) ||
      (this.passwordChecked !== this.passwordChecked2) ||
      (this.genderCheckedFemale !== false && this.genderCheckedMale !== false)
    ) {

      window.alert("Proveri ispravnost unetih podataka!");
      this.updateConf = false;
      return false;
    }

    this.updateConf = true;
    return true;
  }




  private newValues(): void {
    let currentDateAndTime = new Date();

    if (this.firstNameChecked !== "")
      this.user.firstName = this.firstNameChecked;

    if (this.lastNameChecked !== "")
      this.user.lastName = this.lastNameChecked;

    if (this.birthdayChecked !== false) {
      this.user.birthday = this.changeBirthday.value;
    }

    if (this.emailChecked !== "")
      this.user.email = this.emailChecked;


    if (this.usernameChecked !== "")
      this.user.username = this.usernameChecked;

    if (this.passwordChecked !== "") {
      this.user.password = this.passwordChecked;
    }
    if (this.genderCheckedMale !== false) {
      this.user.gender = "male";
    }
    if (this.genderCheckedFemale !== false) {
      this.user.gender = "female";
    }

    const body = {
      username: this.user.username,
      password: this.user.password,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      email: this.user.email,
      imgPath: this.user.imgPath,
      birthday: this.user.birthday,
      gender: this.user.gender,
    };
    //    console.log(body);
    
//    window.alert(this.user._id);
 //   console.log("BODY: ", body);
    const userSub = this.userService.updateUserById(this.user._id, body).subscribe(() => {
      window.alert("Update for  ");
    });
    this.activeSub.push(userSub);

  }


  private agreeToUpd(usernameConf, passwordConf): boolean {
    if ((usernameConf === this.user.username) && (passwordConf === this.user.password))
      return true;
    return false;
  }

  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
  }

  onUpload() {
    const formData = new FormData();
    formData.append('image', this.selectedFile, this.selectedFile.name);
    //ovu putanju promeniti kada se poveze sa bazom! 
    this.http.post('./assets/img/', formData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(event => {
        if (event.type === HttpEventType.Response) {
          console.log(event);
        }
      });
  }

  ngOnDestroy() {

    this.activeSub.forEach((sub: Subscription) => sub.unsubscribe());

  }

}
