import { Component, OnInit, OnDestroy } from '@angular/core';
import { Gym } from '../models/gym.model';
import { Subscription } from 'rxjs';
import { GymService } from '../services/gym.service';
import { ActivatedRoute } from '@angular/router';
import { AddressService } from '../services/address.service';
import { Address } from '../models/address.model';
import { NavbarService } from '../services/navbar.service';

@Component({
  selector: 'app-gym-info',
  templateUrl: './gym-info.component.html',
  styleUrls: ['./gym-info.component.css']
})
export class GymInfoComponent implements OnInit, OnDestroy {

  public gym: Gym;
  private routeSub : Subscription;
  private sub: Subscription;
  public gymId: string;
  public address: Address;
  addressService: any;


  constructor(private route: ActivatedRoute, 
              private gymService: GymService,
              private adsressService: AddressService,
              public navBar: NavbarService) { 

       
              }

  ngOnInit(): void {

    this.routeSub = this.route.params.subscribe(params => {
      this.gymId = params['gymId'];
    });

    this.sub = this.gymService.getGymById(this.gymId)
                               .subscribe(res => {
                                          this.gym = res;
                               });
    this.navBar.showNavbar();
  }

  ngOnDestroy(): void{
    this.routeSub.unsubscribe();
    this.sub.unsubscribe();
  }
}
