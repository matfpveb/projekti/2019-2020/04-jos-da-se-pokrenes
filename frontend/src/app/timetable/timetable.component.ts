import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { Workout } from '../models/workout.model';
import { WorkoutService } from '../services/workout.service';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { NavbarService } from '../services/navbar.service';
import { Subscription } from 'rxjs';
import { GymService } from '../services/gym.service';
import { Gym } from '../models/gym.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TimetableComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public expandedElement: String | null;

  public currentUser: User;
  public client: boolean;
  public users: User[];

  public activeSubs: Subscription[] = [];

  public workouts: Workout[] = [];
  public noOfWorkouts: number;
  public gym: Gym;
  public gymName: string = "";
  public trainer: User;

  public wantDesc: boolean = false;
  public displayWorkouts: boolean = false;

  public displayedColumns: string[] = [];
  public dataSource: MatTableDataSource<Workout>;


  constructor(private userService: UserService,
    private workoutService: WorkoutService,
    private gymService: GymService,
    public navBar: NavbarService,
    private router: Router) {

    this.users = this.userService.getUsers();

    this.currentUser = this.userService.getCurrentUser();

    //client = 1, trainer = 2
    if (this.currentUser.kind === "Trainee") {
      this.client = true;
      this.displayedColumns = ['startTime', 'endTime', 'gym', 'name', 'trainingType', 'buttonResign'];
    } else {
      this.client = false;
      this.displayedColumns = ['startTime', 'endTime', 'gym', 'name', 'currentSpots', 'trainingType', 'buttonUpdate', 'buttonCancel'];

    }
    if (!this.client) {
      const ws = [];
      const sub = this.workoutService.getAllWorkouts()
        .subscribe(res => {
          res.forEach(workout => {
            if (workout && workout.trainer === this.currentUser._id) {
              ws.push(workout);
            }
          });
          this.workouts = ws;
          this.noOfWorkouts = this.workouts.length;
          this.dataSource = new MatTableDataSource(this.workouts);
        });
      this.activeSubs.push(sub);
    } else {

      const sub = this.userService.getUsersWorkouts(this.currentUser._id)
        .subscribe(res => {
          this.workouts = res;
          this.noOfWorkouts = this.workouts.length;
          this.dataSource = new MatTableDataSource(this.workouts);
        });
      this.activeSubs.push(sub);
    }

  }

  ngOnInit(): void {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

    this.navBar.showNavbar();
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach(sub => { sub.unsubscribe() });
  }

  public getTrainer(id: string): User {
    const sub = this.userService.getUserById(id).subscribe(res => {
      this.trainer = res;
    });
    this.activeSubs.push(sub);
    if (this.trainer) {
      return this.trainer;
    }
  }

  public getTrainerUsername(id: string): string {
    this.trainer = this.getTrainer(id);
    if (this.trainer) {
      return this.trainer.username;
    }

  }

  public getGymName(id: string): string {
    const sub2 = this.gymService.getGymById(id).subscribe(res => {
      this.gymName = res.name;
    });
    this.activeSubs.push(sub2);

    console.log(this.gymName);
    return this.gymName;
  }


  public removeWorkout(workout: Workout): void {
    const name = workout.name;
    const sub = this.userService.removeWorkout(workout._id).subscribe(res => { });
    this.activeSubs.push(sub);

    const no = workout.currentSpots - 1;
    const body = { currentSpots: no };
    const sub2 = this.workoutService.updateWorkoutById(workout._id, body).subscribe(() => { });
    this.activeSubs.push(sub2);
    window.alert("Uspesno ste se odjavili sa treninga " + name);
    this.router.navigate(['/main', this.currentUser.username]);
  }

  public cancelWorkout(workout: Workout): void {
    const id = workout._id;
    const name = workout.name;
    const sub = this.workoutService.cancelWorkoutById(id)
      .subscribe(res => { });
    this.activeSubs.push(sub);

    window.alert("Uspešno ste okazali trening " + name);
    this.router.navigate(['/main', this.currentUser.username]);

  }

  public seeDescription(): void {
    this.wantDesc = !this.wantDesc;
  }

  public onDisplayWorkouts(): void {
    this.displayWorkouts = !this.displayWorkouts;
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ////// SORTIRANJE
  sortData(sort: Sort) {
    const data = this.workouts.slice();
    if (!sort.active || sort.direction === '') {
      this.workouts = data;
      this.dataSource = new MatTableDataSource(this.workouts);
      return;
    }

    this.workouts = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'startTime': return compare(a.startTime, b.startTime, isAsc);
        case 'endTime': return compare(a.endTime, b.endTime, isAsc);
        //   case 'gym': return compare(a.gym.name, b.gym.name, isAsc);
        case 'name': return compare(a.name, b.name, isAsc);
        //  case 'lastName': return compare(a.trainer.lastName, b.trainer.lastName, isAsc);
        case 'currentSpots': return compare((a.maxSpots - a.currentSpots), (b.maxSpots - b.currentSpots), isAsc);
        case 'trainingType': return compare(a.trainingType, b.trainingType, isAsc);
        default: return 0;
      }
    });
    this.dataSource = new MatTableDataSource(this.workouts);
  }
}

function compare(a: number | string | Date, b: number | string | Date, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
