import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { NavbarService } from '../services/navbar.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Status } from '../models/status.model';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit, OnDestroy {

  public users: User[];
  public statuses: Status[] = [];
  public statusi: string[];
  public currentStatus: string = null;
  public currentUser: User;

  public subscribedTo: User[] = [];
  public followers: User[] = [];

  private activeSub: Subscription[] = [];
  private paramMapSub: Subscription = null;

  public info: boolean = false;
  public subsExist:boolean = false;

  constructor(private userService: UserService,
    private navService: NavbarService,
    private route: ActivatedRoute) {


    // this.paramMapSub = this.route.paramMap.subscribe(params => {
    //   const pUsername: string = params.get('username');
    //   this.userService.getUsers().forEach(user => {
    //     if (user.username === pUsername) {
    //       this.currentUser = user;
    //     }
    //   });
    // });


    this.currentUser = this.userService.getCurrentUser();
  }

  ngOnInit(): void {
    this.navService.showNavbar();

    const sub = this.userService.getSubs(this.currentUser._id)
      .subscribe(res => {
        this.subscribedTo = res;

        if(res.length !== 0){
          res.forEach(sub => {
            const subs = this.userService.getStatuses(sub._id)
            .subscribe(stats => {
              stats.forEach(stat => {
                this.statuses.push(stat);
              });
            this.activeSub.push(subs);

            if(this.statuses.length !== 0){
              this.info = true;
            }
            else{
              this.subsExist = true;
            }
            });   
          });
          
        }
      });
    this.activeSub.push(sub);

    const sub2 = this.userService.getFollowers(this.currentUser._id)
      .subscribe(res => {
        this.followers = res;
      });
    this.activeSub.push(sub2);
    
  }

  ngOnDestroy() {
    this.activeSub.forEach((sub) => sub.unsubscribe());
  }

  public getUserName(sid:string) {
    let name = "";

    this.subscribedTo.forEach(sub => {
      const found = sub.statuses.find((id) => id === sid);
      if(found){
        name = sub.username;
      }
    });

    return name;
  }

  public sendStatus() {

    const body = {
      date: new Date(),
      content: this.currentStatus
    };

    const subS = this.userService.addStatus(this.currentUser._id, body)
      .subscribe((res) => console.log(res),
        (err) => console.log(err));
    this.activeSub.push(subS);
    this.currentStatus = "";
    
    window.alert("Uspesno je podeljen status!" + this.currentStatus);

    // window.location.reload();
  };



}
