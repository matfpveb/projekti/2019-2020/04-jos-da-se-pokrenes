import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { WorkoutService } from '../services/workout.service';
import { Workout } from '../models/workout.model';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Gym } from '../models/gym.model';
import { User } from '../models/user.model';
import { GymService } from '../services/gym.service';
import { UserService } from '../services/user.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-workouts',
  templateUrl: './all-workouts.component.html',
  styleUrls: ['./all-workouts.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AllWorkoutsComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(MatPaginator, { static: true }) paginator2: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort2: MatSort;

  public expandedElement: String | null;

  public workouts: Workout[];
  public allWorkouts: Workout[];
  public noOfWorkouts: number;

  public dataSourceAll: MatTableDataSource<Workout>;
  public displayedColumnsAll: string[] = [];

  public gym: Gym;
  public gymName: string;
  public trainer: User;

  public client: boolean = false;
  public users: User[];
  public currentUser: User;

  public activeSub: Subscription[] = [];

  private foundWorkout: boolean = false;

  constructor(private workoutService: WorkoutService,
    private gymService: GymService,
    private userService: UserService,
    private router: Router) {

    this.displayedColumnsAll = ['startTime', 'endTime', 'gym', 'name',
      //'trainer', 
      'difficulty', 'currentSpots', 'trainingType', 'price'];

    this.users = this.userService.getUsers();
    this.currentUser = this.userService.getCurrentUser();

    if (this.currentUser.kind === "Trainee") {
      this.client = true;
      this.displayedColumnsAll = ['startTime', 'endTime', 'gym', 'name',
        //   'trainer', 
        'difficulty', 'currentSpots', 'trainingType', 'price', 'buttonApply'];
    }

  }

  ngOnInit(): void {

    const sub = this.workoutService
      .getAllWorkouts()
      .subscribe(res => {
        this.allWorkouts = res;
        this.noOfWorkouts = this.allWorkouts.length;
        this.dataSourceAll = new MatTableDataSource(res);
      });
    this.activeSub.push(sub);
    // this.dataSourceAll.paginator = this.paginator;
    if (this.dataSourceAll) {
      this.dataSourceAll.sort = this.sort2;
      setTimeout(() => this.dataSourceAll.paginator = this.paginator2);
    }

  }

  ngAfterViewInit() {
    if (this.dataSourceAll) {
      this.dataSourceAll.paginator = this.paginator2;
      this.dataSourceAll.sort = this.sort2;
    }
  }

  ngOnDestroy() {
    this.activeSub.forEach((sub) => sub.unsubscribe());
  }


  public getTrainerName(id: string): string {
    const sub = this.userService.getUserById(id)
      .subscribe(res => {
        this.trainer = res;
      });
    this.activeSub.push(sub);
    if (this.trainer) {
      let name = this.trainer.firstName + ' ' + this.trainer.lastName;
    } else {
      let name = "Nepoznato";
    }
    return name;
  }

  public getTrainerUser(id: string): string {
    const sub = this.userService.getUserById(id)
      .subscribe(res => {
        this.trainer = res;
      });
    this.activeSub.push(sub);
    return this.trainer.username;
  }

  public getGymNameById(id: string): string {
    const sub2 = this.gymService.getGymById(id)
      .subscribe(res => {
        this.gym = res;
        this.gymName = res.name;
      });
    this.activeSub.push(sub2);
    return this.gymName;
  }

  public applyToWorkout(workout: Workout) {

    if ((this.currentUser.trainings.find(w => w === workout._id)) !== undefined) {
      window.alert("Već ste prijavljeni na ovaj trening");
      return;
    }
    else if ((workout.maxSpots - workout.currentSpots) === 0) {
      window.alert("Nema slobodnih mesta");
      return;
    } else {
      const name = workout.name;
      this.currentUser.trainings.push(workout._id);
      const ws = this.currentUser.trainings;
      const body = { trainings: ws };
      const sub = this.userService.addWorkoutToUser(this.currentUser._id, body).subscribe((res) => { });
      this.activeSub.push(sub);

      workout.traineesApplied.push(this.currentUser._id);
      const spots = workout.currentSpots + 1;
      const body2 = {
        traineesApplied: workout.traineesApplied,
        currentSpots: spots
      };
      const sub2 = this.workoutService.updateWorkoutById(workout._id, body2).subscribe(() => { });
      this.activeSub.push(sub2);

      window.alert("Uspešno ste se prijavili na trening " + name);
      this.router.navigate(['/main', this.currentUser.username]);
    }
  }


  public isApplied(workout: Workout) {
    
    if ((workout.maxSpots - workout.currentSpots) === 0) {
      return true;
    }else if((this.currentUser.trainings.find(w => w === workout._id)) !== undefined){
      return true;
    }else{
      return false;
    }
  }

  //FILTRIRANJE
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceAll.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceAll.paginator) {
      this.dataSourceAll.paginator.firstPage();
    }
  }

  ////// SORTIRANJE
  sortData2(sort: Sort) {
    const data = this.allWorkouts.slice();
    if (!sort.active || sort.direction === '') {
      this.allWorkouts = data;
      this.dataSourceAll = new MatTableDataSource(this.allWorkouts);
      return;
    }

    this.allWorkouts = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'startTime': return compare(a.startTime, b.startTime, isAsc);
        case 'endTime': return compare(a.endTime, b.endTime, isAsc);
        //   case 'gym': return compare(a.gym.name, b.gym.name, isAsc);
        case 'name': return compare(a.name, b.name, isAsc);
        //   case 'trainer': return compare(this.getTrainerName(a.trainer), this.getTrainerName(b.trainer), isAsc);
        case 'difficulty': return compare(a.difficulty, b.difficulty, isAsc);
        case 'currentSpots': return compare((a.maxSpots - a.currentSpots), (b.maxSpots - b.currentSpots), isAsc);
        case 'trainingType': return compare(a.trainingType, b.trainingType, isAsc);
        case 'price': return compare(a.price, b.price, isAsc);
        default: return 0;
      }
    });
    this.dataSourceAll = new MatTableDataSource(this.allWorkouts);
  }
}

function compare(a: number | string | Date, b: number | string | Date, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
