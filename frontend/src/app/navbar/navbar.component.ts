import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { NavbarService } from '../services/navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

//TODO: bolji nacin za pracenje aktivnog user-a i bug fix za dropdown 

  public username: string;
  private currentUser: User;
  private users: User[];

  constructor(public userService: UserService,
     public navService : NavbarService) { 
      this.currentUser = userService.getCurrentUser(); 
  }

  ngOnInit(): void {
  }

}
