import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ChatHeader } from '../chat-header.model';
import { Observable, from } from 'rxjs';
import { ChatUser } from '../chat-user.model';
import { ChatService } from '../services/chat.service';
import { ChatRoom } from '../chat-room.model';
import { FormControl } from '@angular/forms';
import { filter, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-create-chatroom',
  templateUrl: './create-chatroom.component.html',
  styleUrls: ['./create-chatroom.component.css']
})
export class CreateChatroomComponent implements OnInit {

  @Input()
  public allChatUsers: ChatUser[];

  public selectedChatUsers: ChatUser[] = [];

  @Input()
  public chatRoomName: string;

  @Input()
  public chatRoomDescription: string;

  constructor(private chatService: ChatService) {
  }

  public optionClicked(event: Event, user: ChatUser) {
    event.stopPropagation();
    this.toggleSelection(user);
  }

  public toggleSelection(user: ChatUser) {
    user.selected = !user.selected;

    if (user.selected) {
      this.selectedChatUsers.push(user);
    } else {
      const i = this.selectedChatUsers.findIndex(value => value.username === user.username);
      this.selectedChatUsers.splice(i, 1);
    }
  }

  public onBtnClick(): void{
    const name = this.chatRoomName;
    const description = this.chatRoomDescription;

    const newChatRoom: ChatRoom = {
      name,
      description,
      users: this.selectedChatUsers.map(e => e._id)
    };

    this.chatService.sendMessageCreatedRoom(newChatRoom);

    this.selectedChatUsers.length = 0;
    this.chatRoomDescription = '';
    this.chatRoomName = '';
    this.allChatUsers.forEach(e => e.selected = false);
  }

  ngOnInit(): void {
  }

}
