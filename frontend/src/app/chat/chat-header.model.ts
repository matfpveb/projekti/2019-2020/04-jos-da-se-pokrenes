import { ChatMessage } from './chat-message.model';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

export interface ChatHeader {
  _id: string; // chatRoomId
  name: string;
  username: string;
  lastMessage: string;
  // seen?: boolean;
  time: Date;
}
