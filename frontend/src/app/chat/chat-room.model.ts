export interface ChatRoom{
    _id?: string;
    name: string;
    description?: string;
    users: string[];
}
