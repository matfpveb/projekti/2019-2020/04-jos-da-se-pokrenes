import {
  Component,
  OnInit,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  EventEmitter,
} from '@angular/core';
import { ChatUser, UserStatus } from '../chat-user.model';
import { Observable } from 'rxjs';
import { ChatService } from '../services/chat.service';

import { switchMap, filter, map } from 'rxjs/operators';
import { MatSelectChange } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { ChatEvent } from '../chat-event.model';

@Component({
  selector: 'app-chat-users',
  templateUrl: './chat-users.component.html',
  styleUrls: ['./chat-users.component.css'],
})
export class ChatUsersComponent implements OnInit {
  @Input()
  public chatUsers: ChatUser[] = [];

  @Input()
  public allChatUsers: ChatUser[] = [];

  @Input()
  public unselectedChatUsers: ChatUser[] = [];

  @Output()
  public privateMessage = new EventEmitter<ChatUser>();

  public selectedId = '';

  constructor(private chatService: ChatService) {
    this.chatService.getChatObservable().subscribe({
      next: this.onNewMessage(),
    });
  }

  public onNewMessage() {
    return (chatEvent: ChatEvent) => {
      if (chatEvent.eventType === 'removedUser') {
        this.onNewMessageRemovedUser(chatEvent);
        }
       else if (chatEvent.eventType === 'addedUser') {
         this.onNewMessageAddedUser(chatEvent);
      }
    };
  }

  private onNewMessageAddedUser(chatEvent: ChatEvent): void{
    const userId = chatEvent.value.user.id;
    const currentUserId = localStorage.id;

    if (currentUserId !== userId) {
      const user: ChatUser = {
        _id: userId,
        username: chatEvent.value.user.username,
        iconUrl: chatEvent.value.user.iconUrl,
        firstname: chatEvent.value.user.firstname,
        lastname: chatEvent.value.user.lastname
      };

      this.chatUsers.push(user);
    }
  }

  private onNewMessageRemovedUser(chatEvent: ChatEvent): void {
    const chatRoomId = chatEvent.value.chatRoom.id;
    const userId = chatEvent.value.user.id;

    const currentChatRoomId = this.chatService.openedChatRoomId;
    const currentUserId = localStorage.id;

    if (chatRoomId === currentChatRoomId) {
        if (currentUserId === userId) {
          this.chatService.openedChatRoomName = '';
          this.chatService.openedChatRoomId = '';
          this.chatUsers.length = 0;
        } else {

          const index = this.chatUsers.map(e => e._id).indexOf(userId, 0);
          if (index > -1) {
            this.chatUsers.splice(index, 1);
          }
        }
      }
    }

  public onEnter(input: HTMLInputElement): void {
    if(this.selectedId){
      const user: ChatUser = {
        _id: this.selectedId
      };
      this.chatService.sendMessageAddedUser(user);

      const index = this.unselectedChatUsers.map(e => e._id).indexOf(this.selectedId, 0);
      if (index > -1) {
        this.unselectedChatUsers.splice(index, 1);
      }
    }
    input.value = '';
  }

  public onSelect(selectedOption) {
    this.selectedId = selectedOption._id;
  }

  public getOptionText(obj: any): string {
    return obj.username;
  }

  public onClickRemove(userId: string): void {
    const user = this.chatUsers.find((e) => e._id === userId);

    this.unselectedChatUsers.push(user);

    this.chatService.sendMessageRemovedUser(user);
  }

  public onClickMessage(user: ChatUser): void {
    this.privateMessage.emit(user);
  }

  public AddUserInputReadonly(): boolean {
    return !this.chatService.openedChatRoomId;
  }

  public getAddUserInputLabel(): string {
    if (this.chatService.openedChatRoomId) {
      return 'Select an user to add.';
    } else {
      return 'ChatRoom not selected.';
    }
  }

  ngOnInit(): void {}
}
