import {
  webSocket,
  WebSocketSubject,
  WebSocketSubjectConfig,
} from 'rxjs/webSocket';

import { Injectable } from '@angular/core';
import { ChatEvent } from '../chat-event.model';

@Injectable({
  providedIn: 'root',
})
// TODO: Custom serialization/deserialization? JSON.stringify is used by the dfault
export class ChatWebSocketService {

  private readonly wsUrl = 'ws://localhost:3000';

  private webSocketConnection: WebSocketSubject<ChatEvent>;

  constructor() {
    const  auth = localStorage.jwt;

    // Websocket sends the initial http request that asks for a tcp connection
    // GET ws://example.com:8181/ HTTP/1.1
    // Host: localhost:8181
    // Connection: Upgrade
    // Upgrade: websocket
    // Jwt will be attached to this initial request as a query string

    const config = {
      url: this.wsUrl + '?auth=' + auth
    };

    this.webSocketConnection = webSocket<ChatEvent>(config);
  }

  public emitMessage(message: ChatEvent): void {
    this.webSocketConnection.next(message);
  }

  public getConnection(): WebSocketSubject<ChatEvent> {
    return this.webSocketConnection;
  }
}
