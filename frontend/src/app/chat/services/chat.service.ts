import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable, EMPTY, from } from 'rxjs';

import { ChatHeader } from '../chat-header.model';
import { ChatUser } from '../chat-user.model';
import { ChatMessage } from '../chat-message.model';
import { ChatWebSocketService } from './chat-web-socket.service';
import { ChatEvent } from '../chat-event.model';
import { WebSocketSubject } from 'rxjs/webSocket';
import { ChatRoom } from '../chat-room.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private readonly chatUrl = 'http://localhost:3000/chat/';

  public openedChatRoomId: string = null;
  public openedChatRoomName: string = null;

  constructor(
    private http: HttpClient,
    private webSocketService: ChatWebSocketService
  ) {}

  public sendMessageCreatedRoom(chatRoom: ChatRoom): void{

    console.log(localStorage);
    const chatEvent: ChatEvent = {
      eventType: 'createdRoom',
      value : {
          user: {
          id: localStorage.id
        },
        chatRoom: {
          name: chatRoom.name,
          description: chatRoom.description,
          users: chatRoom.users.map(e => ({id: e}))
        },
      }
    };

    this.webSocketService.emitMessage(chatEvent);
  }

  public sendMessageAddedUser(user: ChatUser): void{
    const chatEvent: ChatEvent = {
      eventType: 'addedUser',
      value : {
          user: {
          id: user._id
        },
        chatRoom: {
          id: this.openedChatRoomId
        },
      }
    };

    this.webSocketService.emitMessage(chatEvent);
  }

  public sendMessageRemovedUser(user: ChatUser): void{
    const chatEvent: ChatEvent = {
      eventType: 'removedUser',
      value : {
          user: {
          id: user._id
        },
        chatRoom: {
          id: this.openedChatRoomId
        },
      }
    };

    this.webSocketService.emitMessage(chatEvent);
  }

  public sendMessage(messageText): void{
    const chatEvent: ChatEvent = {
      eventType: 'message',
      value : {
          user: {
          id: localStorage.id
        },
        chatRoom: {
          id: this.openedChatRoomId
        },
        message:{
          text: messageText
        }
      }
    };

    this.webSocketService.emitMessage(chatEvent);
  }

  public getChatObservable(): WebSocketSubject<ChatEvent>{
    return this.webSocketService.getConnection();
  }

  public getChatHeaders(): Observable<ChatHeader[]> {
    const url = `${this.chatUrl}headers`;
    console.log(url);
    return this.http.get<ChatHeader[]>(url);
  }

  public getChatHistory(): Observable<ChatMessage[]> {
    if (this.openedChatRoomId !== null) {
      const url = `${this.chatUrl}${this.openedChatRoomId}/messages`;

      return this.http.get<ChatMessage[]>(url);
    } else {
      return from([[]]);
    }
  }

  public getChatUsers(): Observable<ChatUser[]> {
    if (this.openedChatRoomId !== null) {
      const url = `${this.chatUrl}${this.openedChatRoomId}/users`;

      return this.http.get<ChatUser[]>(url)
        .pipe(map(e => e.filter(f => f._id !== localStorage.id)));
    } else {
      return from([[]]);
    }
  }

  public getPrivateChatRoom(userId: string): Observable<ChatHeader>{
    const url = `${this.chatUrl}` + 'privateChatRoom';

    const params = new HttpParams({
      fromObject: {
        userIds : [userId, localStorage.id]
        }
    });

    return this.http.get<ChatHeader>(url, {params});
  }

  public getAllUsers(): Observable<ChatUser[]> {
    const url = this.chatUrl + 'all-users';
    return this.http.get<ChatUser[]>(url)
      .pipe(map(e => e.filter(f => f._id !== localStorage.id)));
  }

  public createRoom(chatRoom: ChatRoom): Observable<ChatHeader>{
    return this.http.post<ChatHeader>(this.chatUrl, chatRoom);
  }

  public addUser(userId: string){
    const url = `${this.chatUrl}${this.openedChatRoomId}/addUsers`;

    const body = {
      users: [userId]
    };

    console.log(body);

    return this.http.post(url, body);
  }

  public removeUser(userId: string){
    if (this.openedChatRoomId !== null) {
      console.log(this.openedChatRoomId);
      const url = `${this.chatUrl}${this.openedChatRoomId}/removeUsers`;

      const body = {
        users: [userId]
      };

      return this.http.post(url, body);
    }
  }
}
