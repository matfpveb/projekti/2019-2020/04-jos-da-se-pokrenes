import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchfilter',
})

export class SearchfilterPipe implements PipeTransform {
  transform(value: any, searchValue): any {
    console.log(value);

    if (!searchValue) {
      return value;
    }

    return value.filter(
      (v) => v.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1);
  }
}
