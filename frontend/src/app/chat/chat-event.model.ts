// export interface ChatEvent {
//   userId: string;
//   username?: string;
//   chatRoomId: string;
//   message: string;
// }

export interface ChatEvent {
  eventType: string; // message, addedUser, removedUser, createdRoom
  value: { // Event Value, depending on the type of event it could be user/chatroom/message
    user:
    {
      id: string;
      username?: string;
      iconUrl?: string;
      firstname?: string;
      lastname?: string;
    };
    chatRoom:
     {
      id?: string;
      name?: string;
      description?: string;
      users?: {
        id: string;
        username?: string;
        iconUrl?: string;
      }[],
    };
    message?:
    {
      time?: Date;
      text: string;
    };
  };
}
