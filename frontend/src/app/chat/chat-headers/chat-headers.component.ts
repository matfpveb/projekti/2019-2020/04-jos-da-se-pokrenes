import { UserService } from './../../services/user.service';
import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef, Renderer2 } from '@angular/core';

import { Observable, of } from 'rxjs';

import { ChatHeader } from '../chat-header.model';
import { ChatUser, UserStatus } from '../chat-user.model';
import { ChatMessage } from '../chat-message.model';
import { ChatService } from '../services/chat.service';
import { filter } from 'rxjs/internal/operators/filter';
import { flatMap } from 'rxjs/operators';
import { ChatRoom } from '../chat-room.model';
import { ChatEvent } from '../chat-event.model';

import {update} from 'jdenticon';
@Component({
  selector: 'app-chat-headers',
  templateUrl: './chat-headers.component.html',
  styleUrls: ['./chat-headers.component.css'],
})
export class ChatHeadersComponent implements OnInit {
  public newRoomClicked = false;
  public searchInput = '';

  public chatHeaders: ChatHeader[] = [];
  public allChatUsers: Observable<ChatUser[]>;

  public currentUsername = '';

  @Output()
  public roomSelected = new EventEmitter<{
    chatRoomId: string;
    chatRoomName: string;
  }>();

  @Input()
  public set changedToPrivateChat(user: ChatUser) {
    if (user) {
      this.chatService.getPrivateChatRoom(user._id).subscribe((e) => {
        if (e) {

          this.roomSelected.emit({
            chatRoomId: e._id,
            chatRoomName: e.name,
          });
        } else {
          const newPrivateChatRoom: ChatRoom = {
            name: user.username + ' ' + this.userService.currentUser.username,
            description: 'private',
            users: [user._id],
          };

          this.chatService.sendMessageCreatedRoom(newPrivateChatRoom);
        }
      });
    }
  }

  constructor(private chatService: ChatService, private rend: Renderer2, private userService: UserService) {

      this.currentUsername = userService.currentUser.username;

      this.chatService.getChatObservable().subscribe({
      next: this.onNewMessage(),
    });

      this.chatService.getChatHeaders().subscribe({
        next: (e) => {
          this.chatHeaders.length = 0;
          this.chatHeaders.push(...e);
        },
      });

      this.allChatUsers = this.chatService.getAllUsers();
  }


  public onNewMessage() {
    return (chatEvent: ChatEvent) => {
       if (chatEvent.eventType === 'message') {
         this.onNewMessageMessage(chatEvent);
      }
      else if (chatEvent.eventType === 'removedUser') {
        this.onNewMessageRemovedUser(chatEvent);
      } else if (chatEvent.eventType === 'addedUser') {
        this.onNewMessageAddedUser(chatEvent);
      }
      else if (chatEvent.eventType === 'createdRoom'){
        this.onNewMessageCreatedRoom(chatEvent);
      }
       this.sortHeadersByDate();

       this.updateIdenticons(chatEvent);
    };
  }

  private updateIdenticons(chatEvent: ChatEvent)
  {
    const id = '#svg-' + chatEvent.value.chatRoom.name;
    const username = chatEvent.value.user.username;

    update(id, username);
  }

  private onNewMessageMessage(chatEvent: ChatEvent): void {
    const chatRoomId = chatEvent.value.chatRoom.id;

    const chatHeader = this.chatHeaders
      .find(e => e._id === chatRoomId);

    chatHeader.lastMessage = chatEvent.value.message.text;
    chatHeader.time = chatEvent.value.message.time;

  }

  private onNewMessageCreatedRoom(chatEvent: ChatEvent): void {
    const chatRoomId = chatEvent.value.chatRoom.id;
    const currentUserId = localStorage.id;

    const userId = chatEvent.value.chatRoom.users.find(e => e.id === currentUserId).id;

    if(currentUserId === userId){
      const chatHeader: ChatHeader = {
        _id : chatRoomId,
        name: chatEvent.value.chatRoom.name,
        username: chatEvent.value.user.username,
        lastMessage: chatEvent.value.message.text,
        time: chatEvent.value.message.time,
      };

      this.chatHeaders.push(chatHeader);

      if(currentUserId === chatEvent.value.user.id)
      {
        this.roomSelected.emit({
          chatRoomId: chatHeader._id,
          chatRoomName: chatHeader.name,
        });

        this.newRoomClicked = false;
      }
    }
  }

  private onNewMessageAddedUser(chatEvent: ChatEvent): void {
    const chatRoomId = chatEvent.value.chatRoom.id;
    const userId = chatEvent.value.user.id;

    const currentUserId = localStorage.id;

    if(currentUserId === userId){
      const chatHeader: ChatHeader = {
        _id : chatRoomId,
        name: chatEvent.value.chatRoom.name,
        username: chatEvent.value.user.username,
        lastMessage: chatEvent.value.message.text,
        time: chatEvent.value.message.time,
      };

      this.chatHeaders.push(chatHeader);
      this.chatHeaders = this.chatHeaders;
    }
    else{
      const chatHeader = this.chatHeaders
        .find(e => e._id === chatRoomId);

      chatHeader.lastMessage = chatEvent.value.message.text;
    }
  }

  private onNewMessageRemovedUser(chatEvent: ChatEvent): void {
    const chatRoomId = chatEvent.value.chatRoom.id;
    const userId = chatEvent.value.user.id;

    const currentUserId = localStorage.id;
    console.log('aa');
    if (currentUserId === userId) {
      this.chatHeaders = this.chatHeaders.filter((e) => e._id !== chatRoomId);
    }
    else{
      const chatHeader = this.chatHeaders
        .find(e => e._id === chatRoomId);

      chatHeader.lastMessage = chatEvent.value.message.text;
    }
  }

  public onNewRoomClick(): void {
    this.newRoomClicked = !this.newRoomClicked;
  }

  public onChatHeaderClick(chatRoom: {
    chatRoomId: string;
    chatRoomName: string;
  }): void {
    this.roomSelected.emit(chatRoom);
  }

  public sortHeadersByDate(): void
  {
    this.chatHeaders = this.chatHeaders.sort((x, y) => new Date(y.time).getTime() - new Date(x.time).getTime());
  }

  ngOnInit(): void {}
}
