import { Component, OnInit } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { ChatUser } from '../chat-user.model';

@Component({
  selector: 'app-chat-main',
  templateUrl: './chat-main.component.html',
  styleUrls: ['./chat-main.component.css']
})
export class ChatMainComponent implements OnInit {

  public chatRoomId: string;

  public chatRoomName: string;

  public changeToPrivateChat: ChatUser = null;

  constructor(private chatService: ChatService) { }

  ngOnInit(): void {
  }

  public onPrivateMessage(user: ChatUser){
    this.changeToPrivateChat = user;
  }

  public onRoomSelected(chatRoom: {chatRoomId: string, chatRoomName: string}): void{
    this.chatRoomId = chatRoom.chatRoomId;
    this.chatRoomName = chatRoom.chatRoomName;

    this.chatService.openedChatRoomId = chatRoom.chatRoomId;
    this.chatService.openedChatRoomName = chatRoom.chatRoomName;
  }
}
