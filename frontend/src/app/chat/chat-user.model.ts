export interface ChatUser {
  _id: string;
  username?: string;
  iconUrl?: string;
  status?: UserStatus;
  selected?: boolean;
  firstname?: string;
  lastname?: string;
}

export enum UserStatus{
  Online,
  Offline,
  Away,
  Dnd
}
