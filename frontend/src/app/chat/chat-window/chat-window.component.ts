import {
  Component,
  Input,
  ElementRef,
  ViewChild,
  AfterViewInit,
  AfterViewChecked,
  Output,
  EventEmitter,
} from '@angular/core';
import { ChatMessage } from '../chat-message.model';
import { ChatUser, UserStatus } from '../chat-user.model';

import { ChatService } from '../services/chat.service';
import { ChatEvent } from '../chat-event.model';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.css'],
})
export class ChatWindowComponent implements AfterViewChecked {

  @ViewChild('scrollMe')
  private scrollContainer: ElementRef;

  @Output()
  public privateMessage = new EventEmitter<string>();

  public usersClicked = false;

  // Current chat messages shown in the window
  public chatMessages: ChatMessage[] = [];
  public chatUsers: ChatUser[] = [];
  public unselectedUsers: ChatUser[] = [];

  // TODO: Move this to shared component, currently both window and headers are having same array
  public allChatUsers: ChatUser[] = [];

  public openedChatRoomName = '';

  @Input()
  public set chatRoomId(val: string) {
    this.onWindowChange();
  }

  ngAfterViewChecked(): void {
    this.scrollContainer.nativeElement.scrollTop = 10000000;
  }

  constructor(private chatService: ChatService) {
    // leak?
    this.chatService.getChatObservable().subscribe({
      next: this.onNewMessage(this.chatMessages),
    });
  }

  public onEnter(input: HTMLInputElement): void {
    if (input.value !== '') {
      this.chatService.sendMessage(input.value);

      input.value = '';
    }
  }

  public onPrivateMessage(userId: string){
    this.usersClicked = false;
    this.privateMessage.emit(userId);
  }

  public onNewMessage(chatMessages: ChatMessage[]) {
    return (chatEvent: ChatEvent) => {
      const chatMessage: ChatMessage = {
        chatRoomId: chatEvent.value.chatRoom.id,
        userId: chatEvent.value.user.id,
        username: chatEvent.value.user.username,
        message: chatEvent.value.message.text,
        time: chatEvent.value.message.time
      };

      if(this.chatService.openedChatRoomId === chatEvent.value.chatRoom.id){
        chatMessages.push(chatMessage);
      }
    };
  }

  public onUsersClick() {
    this.usersClicked = !this.usersClicked;
  }

  public onLeaveRoom() {
    const user: ChatUser = {
      _id: localStorage.id
    };

    this.chatService.sendMessageRemovedUser(user);
  }

  public onWindowChange() {
    this.chatService.getChatUsers().subscribe({
      next: (e) => {
        this.chatUsers.length = 0;
        this.chatUsers.push(...e.filter(f => f._id !== localStorage.id));

        this.unselectedUsers = this.allChatUsers.filter(f => !this.chatUsers.map(g => g._id).find(g => g === f._id));
      },
    });

    this.chatService.getChatHistory().subscribe({
      next: (e) => {
        this.chatMessages.length = 0;
        this.chatMessages.push(...e);
      },
    });

    this.chatService
      .getAllUsers()
      .pipe(
        // map((e) =>
        //   e.filter((f) => !this.chatUsers.map((g) => g._id).includes(f._id))
        // )
      )
      .subscribe({
        next: (e) => {
          this.allChatUsers.length = 0;
          this.allChatUsers.push(...e);
        },
      });

    this.openedChatRoomName = this.chatService.openedChatRoomName;
  }

  public messageInputReadonly(): boolean {
    return !this.chatService.openedChatRoomId;
  }

  public messageBelongsToUser(message: ChatMessage): boolean {
    return localStorage.id === message.userId;
  }

  public getMessageInputLabel(): string {
    if (this.chatService.openedChatRoomId) {
      return 'Say hi!';
    } else {
      return 'ChatRoom not selected.';
    }
  }
}
