import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { NavbarService } from '../services/navbar.service';
import { Status } from '../models/status.model';
import { stat } from 'fs';
import { WorkoutService } from '../services/workout.service';
import { Workout } from '../models/workout.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  public users: User[];

  public usersTrainer: User[] = [];
  public usersTrainee: User[] = [];

  public status: string[];

  public workOut: Workout[] = [];

  public currentStatus: string = null;
  public statuses: Status[];

  public currentUser: User;
  public userProfile: User;

  public sameProfile: boolean = false;

  public userFromSub: boolean = false;

  private paramMapSub: Subscription = null;
  private activeSub: Subscription[] = [];
  public sub: Subscription;

  public routeUsername: String;

  constructor(private userService: UserService,
    private workoutService: WorkoutService,
    private route: ActivatedRoute,
    public navBar: NavbarService,
    private router: Router
  ) {

    this.paramMapSub = this.route.paramMap.subscribe(params => {
      this.routeUsername = params.get('username');
    });

    this.currentUser = this.userService.getCurrentUser();

    if (this.currentUser.username === this.routeUsername) {
      this.sameProfile = true;
    }
    else {
      this.sameProfile = false;
    }


    if (this.sameProfile) {
      this.userProfile = this.currentUser;
      this.sub = this.userService.getStatuses(this.currentUser._id)
        .subscribe(res => {
          this.statuses = res;
        });
      this.activeSub.push(this.sub);
    }
    else {

      const sub = this.userService.getUserByUsername(this.routeUsername).subscribe(res => {
        if (res.username !== undefined) {
          this.userProfile = res;
          if (this.currentUser.subscribedTo.find((id) => id === this.userProfile._id) !== undefined) {
            this.userFromSub = true;
          }
          this.sub = this.userService.getStatuses(this.userProfile._id)
            .subscribe(res => {
              this.statuses = res;
            });
          this.activeSub.push(this.sub);
        }

      });
      this.activeSub.push(sub);

    }


  }

  public addSubscribe(userId: string) {
    this.currentUser.subscribedTo.push(userId);
    const subscribes = this.currentUser.subscribedTo;
    const body = {
      subscribedTo: subscribes
    };
    const sub = this.userService.updateUserById(this.currentUser._id, body).subscribe(() => {
      //Potreban nam je sada korisnik koji je zapracen, kako bi njemu dodali pratioca.

      const sub2 = this.userService.getUserById(userId).subscribe((userById) => {
        userById.followers.push(this.currentUser._id);
        const sub3 = this.userService.updateUserById(userId, { followers: userById.followers }).subscribe((status) => {
          console.log(status);
        })
        this.activeSub.push(sub3);
      });
      this.activeSub.push(sub2);
    });
    this.activeSub.push(sub);
  }


  ngOnInit(): void {
    this.navBar.showNavbar();


    const sub = this.userService.getTrainers().subscribe((trainers) => trainers.forEach((trainer) => {
      if ((this.currentUser.subscribedTo.find(trainerId => trainerId === trainer._id)) === undefined) {
        if (trainer._id !== this.currentUser._id) {
          this.usersTrainer.push(trainer);
        }
      }
    }));

    this.activeSub.push(sub);
    const sub2 = this.userService.getTrainee().subscribe((traineeS) => traineeS.forEach((traineeS) => {
      if ((this.currentUser.subscribedTo.find(trainerId => trainerId === traineeS._id)) === undefined) {
        if (traineeS._id !== this.currentUser._id) {
          this.usersTrainee.push(traineeS);
        }
      }
    }));
    this.activeSub.push(sub2);

    if (this.userProfile) {
      this.userProfile.trainings.forEach((traingId) => {

        const sub = this.workoutService.getWorkoutById(traingId).subscribe((workout) => {
          this.workOut.push(workout);
        });
        this.activeSub.push(sub);

      });
    }
  }
  ngOnDestroy(): void {
    if (this.paramMapSub !== null) {
      this.paramMapSub.unsubscribe();
    }
    this.activeSub.forEach(sub => {
      sub.unsubscribe();
    });
  }

  public otpratiKorisnika(id: string) {

    const sub = this.userService.deleteSubFromUser(this.currentUser._id, id).subscribe(() => {
      window.alert("Uspesno ste otpratili korisnika!");
      this.router.navigate(['/main', this.currentUser.username]);
    });
    this.activeSub.push(sub);
  }


  public sendStatus() {
    if (this.currentStatus !== null) {

      const body = {
        date: new Date(),
        content: this.currentStatus
      };

      const subS = this.userService.addStatus(this.currentUser._id, body).subscribe((res) => console.log(res),
        (err) => console.log(err));
      this.activeSub.push(subS);
      this.currentStatus = "";
    }
  }

}
