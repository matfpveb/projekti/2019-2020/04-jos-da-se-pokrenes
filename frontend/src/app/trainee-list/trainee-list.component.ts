import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { NavbarService } from '../services/navbar.service';
import { WorkoutService } from '../services/workout.service';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { Workout } from '../models/workout.model';

@Component({
  selector: 'app-trainee-list',
  templateUrl: './trainee-list.component.html',
  styleUrls: ['./trainee-list.component.css']
})
export class TraineeListComponent implements OnInit {

  public workoutId: string;
  public routeSub: Subscription;
  public clients: User[] = [];
  public workoutName: string;
  public workout: Workout;
  public sub: Subscription;
  public sub2: Subscription;
  public cli: boolean = false;

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private navBar: NavbarService,
              private workoutService: WorkoutService) { 

                if(this.clients.length !== 0){

                  this.cli = true;
                }
                
              }

  ngOnInit(): void {
    
    this.routeSub = this.route.params.subscribe(params => {
      this.workoutId = params['wid'];
    });

    this.sub = this.workoutService.getWorkoutById(this.workoutId)
    .subscribe(res => {
               this.workout = res;
               this.workoutName = res.name;
               console.log(this.workout);
               
               this.sub2 = this.workoutService.getTraineesOnWorkout(this.workoutId)
               .subscribe(res => {
                 this.clients = res;
                 if(this.clients.length !== 0){
                   this.cli = true;
                 }
               });
 
    });
    

    this.navBar.showNavbar();

  }

  ngOnDestroy(): void{
    this.routeSub.unsubscribe();
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
  }
}
