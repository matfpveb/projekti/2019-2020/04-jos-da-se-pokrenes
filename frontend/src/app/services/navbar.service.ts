import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  public visible: boolean;
  constructor() {
    this.visible = false;
   }

   public hideNavbar(){
     this.visible = false;
   }
   public showNavbar(){
     this.visible = true;
   }
   public toggle(){
     this.visible = !this.visible;
   }

}
