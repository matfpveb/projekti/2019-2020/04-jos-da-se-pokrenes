import { Injectable } from '@angular/core';
import { Workout } from '../models/workout.model';
import { User } from '../models/user.model';
import { UserService } from './user.service';
import { Gym } from '../models/gym.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from '../models/address.model';


@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  private allWorkouts: Observable<Workout[]>;
  private readonly ourUrl = 'http://localhost:3000/trainings/';

  private workouts: Workout[];
  private user: User;
  private users: User[];

  constructor(private userService: UserService, private http:HttpClient) {
  
    this.users = this.userService.getUsers();
    this.user = this.userService.getCurrentUser();

    this.allWorkouts = this.http.get<Workout[]>(this.ourUrl);
   }


  //TODO: test it
  public getAllWorkouts(): Observable<Workout[]>{
    return this.allWorkouts;
  }

  public addWorkout(data){
     return this.http.post<any>(this.ourUrl,  data);
    //this.http.post<Workout>(this.ourUrl,  data);
  }

  public getWorkoutById(wid: String): Observable<Workout>{
    return this.http.get<Workout>(this.ourUrl + wid);
  }

  public updateWorkoutById(wid: String, data):Observable<Workout>{
    return this.http.patch<Workout>(this.ourUrl+wid, data);
  }

  
  public getTraineesOnWorkout(wid:String): Observable<User[]>{
    return this.http.get<User[]>(this.ourUrl+ wid + "/trainees");
  }

  public getTrainingTypes():Observable<string[]>{
    return this.http.get<string[]>(this.ourUrl + "types"); 
  }

  public cancelWorkoutById(id:string){
      return this.http.delete<any>(this.ourUrl + id);
  }

  //TODO: verovatno obrisati ovo --> na user-u
  public getWorkoutsByUser(username: string): Workout[]{
    let userWorkouts;
    this.users.forEach(user => {
      if(user.username === username){
        userWorkouts =  user.workouts;
      }
    });
    return userWorkouts;
  }



}