import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Address } from '../models/address.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private readonly adressUrl = "http://localost/addresses/";

  constructor(private http: HttpClient) { }

  public getAddressById(id:string) :Observable<Address> {
    return this.http.get<Address>(this.adressUrl + id);
  }
}
