import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkoutService } from '../services/workout.service';
import { Workout } from '../models/workout.model';
import { GymService } from '../services/gym.service';
import { Gym } from '../models/gym.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavbarService } from '../services/navbar.service';

@Component({
  selector: 'app-workout-update',
  templateUrl: './workout-update.component.html',
  styleUrls: ['./workout-update.component.css']
})
export class WorkoutUpdateComponent implements OnInit {

  private routeSub: Subscription;
  private activeSubs: Subscription[] = [];

  public workoutId: string;
  public workout: Workout;
  public trainingTypes: string[];

  public gymId: string;
  public gym: Gym;
  public gyms: Gym[];
  public gymNames: string[];

  public checkForm: FormGroup;

  public body;

  constructor(private route: ActivatedRoute,
    private workoutService: WorkoutService,
    private gymService: GymService,
    private formBuilder: FormBuilder,
    public navBar: NavbarService,
    private router: Router) {

    this.checkForm = this.formBuilder.group({
      changeName: ['', []],
      changeGym: ['', []],
      changeStartTime: ['', []],
      changeEndTime: ['', []],
      changeMaxSpots: ['', [Validators.min(1)]],
      changeDifficulty: ['', []],
      changeTrainingType: ['', []],
      changeDescription: ['', [Validators.minLength(10), Validators.maxLength(512)]],
      changePrice: ['', [Validators.min(0)]]
    });

    const sub = this.workoutService.getTrainingTypes().subscribe(res => {
      this.trainingTypes = res;
    });

    this.activeSubs.push(sub);

    const sub2 = this.gymService.getGymNames().subscribe(res => {
      this.gymNames = res;
    });
    this.activeSubs.push(sub2);

    // this.body = {
    //   name: this.workout.name,
    //   gym: this.workout.gym,
    //   startTime: this.workout.startTime,
    //   endTime: this.workout.endTime,
    //   maxSpots: this.workout.maxSpots,
    //   difficulty: this.workout.difficulty,
    //   trainingType: this.workout.trainingType,
    //   description: this.workout.description,
    //   price: this.workout.price
    // }

  }

  ngOnInit(): void {

    this.routeSub = this.route.params.subscribe(params => {
      this.workoutId = params['wusername'];
    });

    const sub = this.workoutService.getWorkoutById(this.workoutId)
      .subscribe(res => {
        this.workout = res;
        console.log(this.workout);
        this.gymId = res.gym;
        this.body = {
          name: this.workout.name,
          gym: this.workout.gym,
          startTime: this.workout.startTime,
          endTime: this.workout.endTime,
          maxSpots: this.workout.maxSpots,
          difficulty: this.workout.difficulty,
          trainingType: this.workout.trainingType,
          description: this.workout.description,
          price: this.workout.price
        }
        const sub3 = this.gymService.getGymById(this.gymId)//this.gymId
          .subscribe(res => {
            this.gym = res;
            console.log(this.gym.name);
          });
        this.activeSubs.push(sub3);
      });

    this.activeSubs.push(sub);

    const sub2 = this.gymService.getGyms()
      .subscribe(res => {
        this.gyms = res;
      });

    this.activeSubs.push(sub2);

    const sub4 = this.gymService.getGymNames()
      .subscribe(res => {
        this.gymNames = res;
        console.log(this.gymNames);
      });
    this.activeSubs.push(sub4);

    this.navBar.showNavbar();
  }


  public get changeName() {
    return this.checkForm.get('changeName');
  }

  public get changeGym() {
    return this.checkForm.get('changeGym');
  }

  public get changeStartTime() {
    return this.checkForm.get('changeStartTime');
  }

  public get changeEndTime() {
    return this.checkForm.get('changeEndTime');
  }

  public get changeMaxSpots() {
    return this.checkForm.get('changeMaxSpots');
  }

  public get changeDifficulty() {
    return this.checkForm.get('changeDifficulty');
  }

  public get changeTrainingType() {
    return this.checkForm.get('changeTrainingType');
  }

  public get changeDescription() {
    return this.checkForm.get('changeDescription');
  }

  public get changePrice() {
    return this.checkForm.get('changePrice');
  }


  public submitForm(data): void {
    console.log(data);
    if (!this.checkForm.valid) {
      window.alert("Not valid! ");
      return;
    }

    //Provere za vreme
    if (this.changeStartTime.value !== "" && this.changeEndTime.value !== "") {
      if (this.changeStartTime.value >= this.changeEndTime.value) {
        window.alert("Ne može termin početka biti kasniji/isti od termina kraja!");
        return;
      }
    } else if (this.changeStartTime.value !== "" && this.changeStartTime.value >= this.workout.endTime) {
      window.alert("Ne može novi termin početka biti kasniji/isti od termina kraja!");
      return;
    } else if (this.changeEndTime.value !== "" && this.changeEndTime.value <= this.workout.startTime) {
      window.alert("Ne može novi termin kraja biti raniji/isti od termina početka!");
      return;
    }

    if ((this.changeStartTime.value !== "" && this.changeStartTime.value < new Date()) ||
      (this.changeEndTime.value !== "" && this.changeEndTime.value < new Date())) {
      window.alert("Ne mogu se birati termini pre trenutnog datuma");
      return;
    }

    //Provera za broj mesta
    if (this.changeMaxSpots.value !== "" && this.changeMaxSpots.value < this.workout.currentSpots) {
      window.alert("Kapacitet je manji od broj trenutno prijavljenih!");
      return;
    }

    if (data.changeName !== "") {
      this.body.name = data.changeName;
    }
    if (data.changeGym !== "") {
      this.gyms.forEach(g => {
        if (g.name === data.changeGym) {
          this.body.gym = g._id;
        }
      });
    }
    if (data.changeStartTime !== "") {
      this.body.startTime = data.changeStartTime;
    }
    if (data.changeEndTime !== "") {
      this.body.endTime = data.changeEndTime;
    }
    if (data.changeMaxSpots !== "") {
      this.body.maxSpots = data.changeMaxSpots;
    }
    if (data.changeTrainingType !== "") {
      this.body.trainingType = data.changeTrainingType;
    }
    if (data.changeDescription !== "") {
      this.body.description = data.changeDescription;
    }
    if (data.changePrice !== "") {
      this.body.price = data.changePrice;
    }

    //update
    const sub = this.workoutService.updateWorkoutById(this.workoutId, this.body)
      .subscribe(() => { });
    this.activeSubs.push(sub);

    this.checkForm.reset();
    window.alert("Uspešno ste izmenili trening:" + this.body.name);
    this.router.navigate(['/timetable']);
  }

  ngOnDistroy(): void {
    this.routeSub.unsubscribe();
    if (this.activeSubs) {
      this.activeSubs.forEach(sub => { sub.unsubscribe(); });
    }
  }
}
