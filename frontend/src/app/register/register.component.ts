import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

  public user: User[];
  public checkForm: FormGroup;
  public trainerChecked: boolean = false;
  public genderChecked: boolean = false;

  public genderInfo: string = "male";
  public positionInfo: string = "Trainee";


  private userSub: Subscription;

  selectedFile: File = null;


  hide = true;
  hide1 = true;


  constructor(private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient) {
    this.checkForm = formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('[A-Z][a-z]+')]],
      lastName: ['', [Validators.required, Validators.pattern('([A-Z][a-z]+)+')]],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.pattern('[_a-zA-Z0-9]+')]],
      password: ['', [Validators.required, Validators.pattern('[a-zA-Z]+[0-9]+')]],
      password2: ['', [Validators.required, Validators.pattern('[a-zA-Z]+[0-9]+')]],
      birthday: new Date()

    })
  }

  ngOnInit(): void {
  }

  public get email() {
    return this.checkForm.get('email');
  }

  public get firstName() {
    return this.checkForm.get('firstName');
  }
  public get lastName() {
    return this.checkForm.get('lastName');
  }

  public get password() {
    return this.checkForm.get('password');
  }

  public get password2() {
    return this.checkForm.get('password2');
  }
  public get birthday() {
    return this.checkForm.get('birthday');
  }

  public get username() {
    return this.checkForm.get('username');
  }

  public submitForm(data): void {
    console.log(data);
    if (!this.checkForm.valid) {
      window.alert("Not valid! ");
      return;
    }
    if (this.password.value !== this.password2.value) {
      window.alert("Šifra se ne poklapa! Pokušajte ponovo! ");
      return;
    }

    const body = {
      "kind": this.positionInfo,
      "username": data.username,
      "password": data.password,
      "firstName": data.firstName,
      "lastName": data.lastName,
      "gender": this.genderInfo,
      "birthday": data.birthday,
      "email": data.email,
      // "imgPath" : data.image
    }
    this.userSub = this.userService.addUser(body).subscribe((user: User) => {
      console.log("Uspesno ste registrovani! ");
      this.userService.putCurrentUser(user);
    });

    this.checkForm.reset();
    this.router.navigate(['/succReg', data.username]);
  }

  public trainerCheckedFunc(): void {
    this.trainerChecked = !this.trainerChecked;

    if (this.trainerChecked === false) {
      this.positionInfo = "Trainee";
      return;
    }
    this.positionInfo = "Trainer";

  }

  public genderCheckedFunc(): void {
    this.genderChecked = !this.genderChecked;

    if (this.genderChecked === false) {
      this.genderInfo = "male";
      return;
    }
    this.genderInfo = "female";

  }

  //ubacivanje slike u bazu :  

  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
  }

  onUpload() {
    const formData = new FormData();
    formData.append('image', this.selectedFile, this.selectedFile.name);
    //ovu putanju promeniti kada se poveze sa bazom! 
    this.http.post('./assets/img/', formData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(event => {
        if (event.type === HttpEventType.Response) {
          console.log(event);
        }
      });
  }


  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

} 
