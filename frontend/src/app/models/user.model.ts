import { Workout } from './workout.model';
import { Status } from './status.model';

export class User {

    constructor(
                public _id: string,
                public username: string,
                public password: string,
                public firstName: string,
                public lastName: string,
                public email: string,
                public credits: number,
                public imgPath: string,  
                public birthday: Date,
                
                public gender: string,
                //1 - klijent, 2 - Trener
                public clientOrTrainer: number, 
                public statuses: string[],
                public subscribedTo: string[] = [],
                public followers: string[] = [],
                public workouts: Workout[] = [], //obrisati kad se ukloni sa svih mesta
                public trainings: string[] = [],
                public kind: string,
                ){
                    

    }
    public getFirsName(): string{
        return this.firstName;
    }

    public getLastName(): string {
        return this.lastName;
    }

    public getBirthday(): Date{
        return this.birthday;
    }

    public getUsername(): string{
        return this.username;
    }
}