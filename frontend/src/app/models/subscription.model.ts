import { User } from './user.model';
import { Workout } from './workout.model';

export class Status {
    constructor(public user: User,
                public followers: User[],
                public subscribedTo: User[],
                public workouts: Workout[]
                ) {
    }
}