import { User } from './user.model';

export class Status {
    constructor(public _id: string,
                public user: User,
                public date: Date,
                public content: string
                ) {
    }
}