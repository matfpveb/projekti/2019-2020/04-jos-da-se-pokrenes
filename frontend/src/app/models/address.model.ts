export class Address {
    constructor(public _id: string,
                public streetName: string,
                public streetNumber: number,
                public city: string,
                public country: string,
                public zip: number,
                ) {
    }
}