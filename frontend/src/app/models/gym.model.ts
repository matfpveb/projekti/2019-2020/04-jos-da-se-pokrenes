import { Address } from './address.model';

export class Gym {
    constructor(public _id: string,
                public name: string,
                public address: Address, //Adress
                public phone: string,
                public startTime: Date,
                public endTime: Date,
                ) {
    }
}