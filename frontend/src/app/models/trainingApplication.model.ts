import { Workout } from './workout.model';
import { Trainee } from './trainee.model';

export class TrainingApplication {
    constructor(
        public training: string , // Training (to je zapravo Workout na fronut)
        public trainee : Trainee,
        public status : string
    ) {         
    }
}