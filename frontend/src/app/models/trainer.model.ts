import {Workout} from './workout.model';
export class Trainer{
    constructor(
        public trainings: string, //training
        public sports: string,
        public rating: number
    ){}
}