const mongoose = require("mongoose");

const dataBaseConnecting = async (db = "just-move-it") => {
    try {
        await mongoose.connect(`mongodb://localhost:27017/${db}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
};

module.exports = dataBaseConnecting;
