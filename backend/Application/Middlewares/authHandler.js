const strategies = require('../config/authStrategies');
const passport = require("passport")
const passportConfiguratin = require("../config/passport");

const secretKey = require('../config/secretkey.json');
const jwt = require('jsonwebtoken');

const localLoginStrategy = strategies.localLogin;
const jwtStrategy = strategies.jwt;

const User = require('../../Models/UserModel');

passport.use('login', localLoginStrategy);
passport.use('jwt', jwtStrategy);

// Middleware function for logging in
// Uses the local strategy for user authentication and token creation 
module.exports.localLoginHandler = async function (req, res, next) {
    //passport.authenticate returns a callback that we want to call with (req, res, next)
    //                                      (err, user, info) params are retrieved from local strategy
    passport.authenticate('login', async function (err, user, info) {
        try{
            if(err){
                return next(err);
            }
            
            if(!user){
                return res.send({message : "Auth failed. " + info.message });
            }
    
            req.logIn(user, async err => {
                if (err) {
                    return next(err);
                }
    
                const jwtPayload = {
                    id: user.id
                };

                const jwtOptions = {
                    expiresIn : 42*42*42*42*42*42*42 // TODO: Change to 30 mins
                };
    
                const token = jwt.sign(jwtPayload,
                     secretKey.secretkey,
                     jwtOptions);

                const _user = await User.getByUsername(user.username);
    
                return res.status(200).json({
                    auth: true,
                    user: _user,
                    token: token,
                    message: "Authentication successful. User logged in."
                });
            });
        }
        catch(err){
            next(err);
        }
    })(req, res, next);
};

// This callback function acts as a middleware that verifies jwt token payload against our data persistence (this is done in jwt strategy)
// and attaches the retrived user (if there's one) to req
module.exports.jwtAuthenticate = async function(req, res, next){
    passport.authenticate('jwt', async (err, user, info) => {
        try{
            if(err){
                return next(err)
            }

            if(!user){
                return res.status(401).json({
                        message: "Authorization failed. " + info.message 
                });
            }
            // Associate a logged in user with current request and move onto the next middleware
            req.user = user;

            return next();
        }
        catch(err){
            return next(err);
        }

    })(req, res, next);
}

//TODO: Make general authentication handling function that'll check roles and allowed actions (that's the one above)
// Used for auth testing
module.exports.jwtAuthenticationTest = async function(req, res, next){
        try{            
            const user = req.user; // An user attached to the request in AuthorizationHandler middleware
            
            const accessedUserId = req.params.id;
            
            const accessedUser = await User.getById(accessedUserId);
    
            // Check if the id from the token payload is the same as the one request is trying to access /users/:accessedUserId
            // functionally, user is the only one who can access his own "profile"
            if(User.hasPermissions('view-profile', {user: user, profile: accessedUser })){
                return res.status(200).json(user);
            }
            else{
                return res.status(401).json({
                    message: `${user.username} can't access profile of the user ${accessedUser.username}.`
                })
            }
        }
        catch(err){
            next(err);
        }
};



