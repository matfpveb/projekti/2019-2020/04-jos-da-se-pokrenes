const WebSocketServer = require('websocket').server;
const chatHandlers = require('./ChatHandlers');

const httpServer = require('../server');

const webSocketServer = new WebSocketServer({
    httpServer: httpServer
});

webSocketServer.on('connect', chatHandlers.onConnect);
webSocketServer.on('close', chatHandlers.onClosed);
webSocketServer.on('request', chatHandlers.onRequest);


module.exports = webSocketServer;
