module.exports.getJson = (message) => {
    const messageJson = JSON.parse(message);

    return messageJson;
};

module.exports.getMessage = (eventType, chatRoom, user, message) => {
    
    const messageJson = {
        eventType,
        value: {
            user: {
                id: user._id,
                username: user.username,
                iconUrl: "todo"
            },
            chatRoom: {
                id: chatRoom.id,
                name: chatRoom.name
            },
            message:{
                text: message.message,
                time: message.time,
            }
        }
    }

    return JSON.stringify(messageJson);
};


// skracena verzija sa klijenta
// chatevent = {
//     eventType : 'addedUser', 'removedUser', 'message' ('seen?')
//     value: {
//         user: {
//             id,
//         }
//         chatroom: {
//             id,
//         }
//         message:{
//             text
//         }
//     }
// }



// sa servera

// chatevent = {
//     eventType : 'addedUser', 'removedUser', 'message' ('seen?')
//     value: {
//         user: {
//             id,
//             username,
//             icon
//         }
//         chatroom: {
//             id,
//             name
//         }
//         message:{
//             time
//             text
//         }
//     }
// }