const session = require("express-session");
const bodyParser = require("body-parser");
const passport = require("passport");

const express = require("express");
const app = express();

const dataBaseConnecting = require('./database');
dataBaseConnecting()
.then(e => console.log("Connected to the database"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//app.use(session({ secret: "cats" }));
app.use(passport.initialize());
//app.use(passport.session());

// CORS Filter
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");

    if (req.method === "OPTIONS") {
        res.header(
            "Access-Control-Allow-Methods",
            "OPTIONS, GET, POST, PATCH, DELETE"
        );
        return res.status(200).json({});
    }
    next();
});

//TMP

//const mockData = require("../Models/MockData");

//mockData.initMockData().then((e) => console.log("Mock Data initialized"));

//

const userRouter = require("../Routers/UserRouter");
const addressRouter = require("../Routers/AddressRouter");
const traineeRouter = require("../Routers/TraineeRouter");
const trainerRouter = require("../Routers/TrainerRouter");
const trainingRouter = require("../Routers/TrainingRouter");
const gymRouter = require("../Routers/GymRouter");
const chatRouter = require("../Routers/ChatRouter");
const statusRouter = require("../Routers/StatusRouter");

app.use("/users", userRouter);
app.use("/addresses", addressRouter);
app.use("/trainees", traineeRouter);
app.use("/trainers", trainerRouter);
app.use("/trainings", trainingRouter);
app.use("/gyms", gymRouter);
app.use("/chat", chatRouter);
app.use("/statuses", statusRouter);

const notFoundHandler = require("./Middlewares/NotFoundHandler");
const errorHandler = require("./Middlewares/errorHandler");

app.get("/*", notFoundHandler);
app.use(errorHandler);

module.exports = app;
