const express = require('express');
const router = express.Router();

const controller = require('../Controllers/GymController');

router.get('/', controller.getAllGyms);
router.post('/', controller.addGym);

router.get('/names', controller.getAllGymNames);

router.get('/:id', controller.getGymById);
router.put('/:id', controller.updateGymById);
router.delete('/:id', controller.removeGymById);

module.exports = router;