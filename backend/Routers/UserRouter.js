const express = require('express');
const router = express.Router();

const controller = require('../Controllers/UserController');

const authMiddleware = require('../Application/Middlewares/AuthHandler');

router.get('/', controller.getAllUsers);

router.post('/login', controller.loginUser);

router.get('/filterByUsername', controller.getUserByUsername);

//TODO: Delete
// We can chain middleware callbacks inside one hit route
//router.get('/:id', authMiddleware.jwtAuthenticate, controller.testAuthentication);
router.get('/:id', controller.getUserById);
router.post('/register', controller.registerUser);
 
router.post('/stat/:id', controller.addStatusToUser);

router.get('/:id/statuses', controller.getAllStatusesFromUser);
router.get('/:id/followers', controller.getAllFollowersByUserId);
router.get('/:id/subscribers', controller.getAllSubscribersByUserId);

router.get('/:userId/:statusId', controller.getStatusFromUser);
router.delete('/:userId/:statusId', controller.removeStatusFromUser);

router.post('/:userId/subscribe/:subscriberId', controller.addSubscriberToUser);
router.delete('/:userId/subscribe/:subscriberId', controller.deleteSubscriberFromUser);

router.patch('/:id', controller.updateUserById);

module.exports = router;