const express = require('express');
const router = express.Router();

const controller = require('../Controllers/StatusController');

router.get("/", controller.getAllStatuses);
router.post("/", controller.addStatus);

router.get("/:id", controller.getStatusById);
router.delete("/:id", controller.removeStatusById);

module.exports = router;