const express = require('express');
const router = express.Router();

const authMiddleware = require('../Application/Middlewares/AuthHandler');

const controller = require('../Controllers/ChatController');

router.post('/', authMiddleware.jwtAuthenticate, controller.createChatRoom);

router.get('/all-users', controller.getAllUsers);
router.get('/privateChatRoom', controller.getPrivateChatRoomId)

router.get('/headers', authMiddleware.jwtAuthenticate, controller.getChatRoomHeaders);

router.get('/:id', controller.getChatRoomWindow);
router.get('/:id/messages', controller.getMessages);
router.get('/:id/users', controller.getUsers);

router.post('/:id/addUsers', authMiddleware.jwtAuthenticate, controller.addUsers);
router.post('/:id/removeUsers', authMiddleware.jwtAuthenticate, controller.removeUsers);

module.exports = router;