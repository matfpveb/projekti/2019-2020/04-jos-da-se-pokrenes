const notFound = require('../Application/Middlewares/NotFoundHandler');

const Status = require('../Models/StatusModel');

module.exports.getAllStatuses = async(req, res, next) => {
    try {
        const statuses = await Status.getAll();
        res.send(statuses);
    } catch(err) {
        next(err);
    }
};

module.exports.addStatus = async(req, res, next) => {
    try {
        const status = await Status.add(req.body);
        res.send(status);
    } catch(err) {
        next(err);
    }
};

module.exports.getStatusById = [ async(req, res, next) => {
    try {
        const status = await Status.getById(req.params.id);
        res.send(status);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.removeStatusById = [ async(req, reds, next) => {
    try {
        await Status.removeById(req.params.id);
        res.send({_id: req.params.id});
    } catch(err) {
        next(err);
    }
},
notFound
];