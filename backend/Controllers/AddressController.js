const notFound = require('../Application/Middlewares/NotFoundHandler');

const Address = require('../Models/AddressModel');

module.exports.getAllAddresses = async(req, res, next) => {
    try {
        const addresses = await Address.getAll();
        res.send(addresses);
    } catch (err) {
        next(err);
    }
};

module.exports.getAddressById = [ async(req, res, next) => {
    try {
        const address = await Address.getById(req.params.id);
        res.send(address);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.addAddress = async(req, res, next) => {
    try {
        address = await Address.add(req.body);
        res.send(address);
    } catch (err) {
        next(err);
    }
};

module.exports.updateAddressById = [ async(req, res, next) => {
    try {
        const address = await Address.updateById(req.params.id, req.body);
        res.send(address);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.removeAddressById = [ async(req, res, next) => {
    try {
        await Address.removeById(req.params.id);
        res.send({ msg: "Address is succesfully deleted!"});
    } catch (err) {
        next(err);
    }
},
notFound
];