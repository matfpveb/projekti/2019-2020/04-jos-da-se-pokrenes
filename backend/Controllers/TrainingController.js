const notFound = require('../Application/Middlewares/NotFoundHandler');

const Training = require('../Models/TrainingModel');

const Trainee = require('../Models/TraineeModel')

module.exports.getAllTrainings = async(req, res, next) => {
    try {
        const trainings = await Training.getAll();
        res.send(trainings);
    } catch (err) {
        next(err);
    }
};

module.exports.getTrainingById = [ async(req, res, next) => {
    try {
        const training = await Training.getById(req.params.id);
        res.send(training);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.updateTrainingById = [ async(req, res, next) => {
    try{
        const id = req.params.id;

        const training = req.body;
        
        training._id = id;
        
        updatedTraining = await Training.checkAndUpdate(training);

        res.send(updatedTraining);
    } catch(err){

        next(err);
    }
},
notFound
];

module.exports.addTraining = async(req, res, next) => {
    try{
        //TODO: Validation?

        const training = req.body;

        const addedTraining = await Training.add(training);

        res.send(addedTraining);
    } catch(err){
        next(err);
    }
};

module.exports.removeTrainingById = [ async(req, res, next) => {
    try {
        await Trainee.removeTraining(req.params.id);
        await Training.removeById(req.params.id);
        res.send({_id: req.params.id});
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.getTraineesById = [ async(req, res, next) => {
    try {
        const trainees = await Training.getAllTraineesByTrainingId(req.params.id);
        res.send(trainees);
    } catch(err) {
        next(err);
    }
},
notFound
];

module.exports.getAllTrainingTypes = async(req, res, next) => {
    try {
        const types = await Training.getAllTypes();
        res.send(types);
    } catch (err) {
        next(err);
    }
};