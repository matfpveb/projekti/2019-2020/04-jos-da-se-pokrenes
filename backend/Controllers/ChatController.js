const notFound = require('../Application/Middlewares/NotFoundHandler');

const ChatRoom = require('../Models/ChatRoomModel');
const Message = require('../Models/MessageModel');
const User = require('../Models/UserModel');

module.exports.getChatRoomWindow = async (req, res, next) => {
    try{
        const chatRoomId = req.params.id;

        const chatWindow = await ChatRoom.getChatRoomHistoryById(chatRoomId);

        if(chatWindow){
            res.status(200).json(chatWindow);
        }
        else{
            res.status(404).json({
                message: "Not found"
            });
        }
    }catch (err){
        next(err);
    }
};

module.exports.createChatRoom = async (req, res, next) => {
    try{
        const createdBy = req.user;
        
        const name = req.body.name;
        const users = req.body.users;
        
        const createdRoom = await ChatRoom.createChatRoom(name, createdBy, users);
        
        
        if(createdRoom){
            const chatRoomHeader = await ChatRoom.getChatRoomHeader(createdRoom._id);
    
            const chatHeaderReadModels =  {
                    _id: chatRoomHeader.id,
                    name: chatRoomHeader.name,
                    username: chatRoomHeader.messages.length < 1 ? null : 
                        (chatRoomHeader.messages[0].user? chatRoomHeader.messages[0].user.username : null),
                    lastMessage: chatRoomHeader.messages.length < 1 ? null : chatRoomHeader.messages[0].message,
                    seen: false
                };

            res.status(201).json(chatRoomHeader);
        }
        else{
            res.status(400).json({
                message: "Failed to created a chat room."
            });
        }
    }catch(err){
        next(err);
    }
};

module.exports.addUsers = async (req, res, next) => {
    try{
        const chatRoomId = req.params.id;

        const users = req.body.users;
        
        await ChatRoom.addUsers(chatRoomId, users);

        const chatRoom = await ChatRoom.getChatRoomById(chatRoomId);
        
        if(!chatRoom){
            res.status(404).json({
                message: "Not found"
            });
        }
        else{
            res.status(200)
                .json(chatRoom);
        }
    }catch(err){
        next(err);
    }
};


module.exports.getPrivateChatRoomId = async (req, res, next) =>{
    try{
        const userIds = req.query.userIds;
        
        const chatRoom = await ChatRoom.getPrivateChatRoom(userIds);
        console.log(chatRoom);
        
        if(!chatRoom){
            res.status(200)
                .json(null);
        }
        else{
            const chatRoomHeader = {
                    _id: chatRoom.id,
                    name: chatRoom.name,
                    username: chatRoom.messages.length < 1 ? null : 
                        (chatRoom.messages[0].user ? chatRoom.messages[0].user.username : null),
                    lastMessage: chatRoom.messages.length < 1 ? null : chatRoom.messages[0].message,
                    time: chatRoom.messages.time < 1 ? null : chatRoom.messages[0].time,
                    seen: false
            };

            res.status(200)
                .json(chatRoomHeader);
        }
    }catch(err){
        next(err);
    }
};

module.exports.removeUsers = async (req, res, next) => {
    try{  
        const chatRoomId = req.params.id;
        
        const users = req.body.users;
        
        await ChatRoom.removeUsers(chatRoomId, users);
        
        const chatRoom = await ChatRoom.getChatRoomById(chatRoomId);
        
        if(!chatRoom){
            res.status(404).json({
                message: "Not found"
            });
        }
        else{
            res.status(200)
                .json(chatRoom);
        }
    }catch(err){
        next(err);
    }
};

module.exports.getMessages = async (req, res, next) => {
    try{
        const chatRoomId = req.params.id;

        const messages = await ChatRoom.getMessages(chatRoomId)

        if(messages){

            const messageReadModels = messages.map(e => {
                return {
                    _id: e.id,
                    chatRoomId: e.chatRoom.id,
                    userId: e.user ? e.user.id : null,
                    username: e.user ? e.user.username : null,
                    message: e.message,
                    time: e.time
                };
            });

            res.status(200)
                .json(messageReadModels);
        }
        else{
            res.status(404).json({
                message: "Not found"
            });
        }
    }catch(err){
        next(err);
    }
};

module.exports.getUsers = async (req, res, next) => {
    try{
        const chatRoomId = req.params.id;

        const users = await ChatRoom.getUsers(chatRoomId);
        if(users){

        const userReadModel = users.map(e =>{
            return {
                _id: e.id,
                username: e.username,
                iconUrl: 'https://TODO:.com',
                status: 0, // TODO:
                firstname: e.firstName,
                lastname: e.lastName
            }
        });
            res.status(200)
                .json(userReadModel);
        }
        else{
            res.status(404).json({
                message: "Not found"
            });
        }
    }catch(err){
        next(err);
    }
};

module.exports.getChatRoomHeaders = async (req, res, next) => {
    try{
        const user = req.user;
        
        const chatHeaders = await ChatRoom.getChatRoomHeaders(user.id);

        if(chatHeaders){

            const chatHeaderReadModels = chatHeaders.map(e => {
                return {
                    _id: e.id,
                    name: e.name,
                    username: e.messages.length < 1 ? null :
                        (e.messages[0].user? e.messages[0].user.username : null),
                    lastMessage: e.messages.length < 1 ? null : e.messages[0].message,
                    time: e.messages.length < 1 ? null : e.messages[0].time,
                    seen: false
                };
            }).sort((x, y) => y.time.valueOf() - x.time.valueOf());

            res.status(200)
                .json(chatHeaderReadModels);
        }
        else{
            res.status(404).json({
                message: "Not found"
            });
        }
    }catch(err){
        next(err);
    }
};

module.exports.getAllUsers = async (req, res, next) => {
    try{
        const users = await User.getAll()
        
        if(users){

        const userReadModel = users.map(e =>{
            return {
                _id: e.id,
                username: e.username,
                iconUrl: 'https://TODO:.com',
                status: 0, // TODO:
                firstname: e.firstName,
                lastname: e.lastName
            }
        });
            res.status(200)
                .json(userReadModel);
        }
        else{
            res.status(404).json({
                message: "Not found"
            });
        }
    }catch(err){
        next(err);
    }
};

module.exports.kickUsers = () => {}; 

module.exports.enterChat = () => {}; // ?


