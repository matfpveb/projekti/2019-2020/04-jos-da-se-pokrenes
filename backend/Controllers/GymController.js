const notFound = require('../Application/Middlewares/NotFoundHandler');

const Gym = require('../Models/GymModel');

module.exports.getAllGyms = async(req, res, next) => {
    try {
        const gyms = await Gym.getAll();
        res.send(gyms);
    } catch (err) {
        next(err);
    }
};

module.exports.getGymById = [ async(req, res, next) => {
    try {
        const gym = await Gym.getById(req.params.id);
        res.send(gym);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.addGym = async(req, res, next) => {
    try {
        const gym = await Gym.add(req.body);
        res.send(gym);
    } catch (err) {
        next(err);
    }
};

module.exports.updateGymById = [ async(req, res, next) => {
    try {
        const gym = await Gym.updateById(req.params.id, req.body);
        res.send(gym);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.removeGymById = [ async(req, res, next) => {
    try {
        await Gym.removeById(req.params.id);
        res.send({ msg: "Gym is succesfully deleted!"});
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.getAllGymNames = async(req, res, next) => {
    try {
        const gyms = await Gym.getAllNames();
        res.send(gyms);
    } catch (err) {
        next(err);
    }
};