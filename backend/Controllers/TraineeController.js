const notFound = require('../Application/Middlewares/NotFoundHandler');

const Trainee = require('../Models/TraineeModel');

const Training = require('../Models/TrainingModel');

module.exports.getAllTrainees = async(req, res, next) => {
    try {
        const trainees = await Trainee.getAll();
        res.send(trainees);
    } catch (err) {
        next(err);
    }
};

module.exports.getTraineeById = [ async(req, res, next) => {
    try {
        const trainee = await Trainee.getById(req.params.id);
        res.send(trainee);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.updateTraineeById = [ async(req, res, next) => {
    try{
        const id = req.params.id;

        const trainee = req.body;
        
        trainee._id = id;
        
        updatedTrainee = await Trainee.checkAndUpdate(trainee);

        res.send(updatedTrainee);
    } catch(err){
        next(err);
    }
},
notFound
];

module.exports.addTrainee = async(req, res, next) => {
    try{
        //TODO: Validation?

        const trainee = req.body;

        const addedTrainee = await Trainee.add(trainee);

        res.send(addedTrainee);
    } catch(err){
        next(err);
    }
};

module.exports.removeTraineeById = [ async(req, res, next) => {
    try {
        await Trainee.removeById(req.params.id);
        
        res.send({_id: req.params.id});
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.unsubscribeFromTrainingByTrainingId = [ async(req, res, next) => {
    try {
        await Trainee.unsubscribeFromTrainingByTrainingId(req.params.traineeId, req.params.trainingId);

        res.send({ message: "Training removed from trainee!" });
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.getAllTrainingsFromTraineeById = async(req, res, next) => {
    try {
        const trainings = await Trainee.getAllTrainings(req.params.id);

        res.send(trainings);
    } catch(err) {
        next(err);
    }
};