const notFound = require('../Application/Middlewares/NotFoundHandler');

const Trainer = require('../Models/TrainerModel');

module.exports.getAllTrainers = async(req, res, next) => {
    try {
        const trainers = await Trainer.getAll();
        res.send(trainers);
    } catch (err) {
        next(err);
    }
};

module.exports.getTrainerById = [ async(req, res, next) => {
    try {
        const trainer = await Trainer.getById(req.params.id);
        res.send(trainer);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.updateTrainerById = [ async(req, res, next) => {
    try{
        const id = req.params.id;

        const trainer = req.body;
        
        trainer._id = id;
        
        updatedTrainer = await Trainer.checkAndUpdate(trainer);

        res.send(updatedTrainer);
    } catch(err){
        next(err);
    }
},
notFound
];

module.exports.addTrainer = async(req, res, next) => {
    try{
        //TODO: Validation?

        const trainer = req.body;

        const addedTrainer = await Trainer.add(trainer);

        res.send(addedTrainer);
    } catch(err){
        next(err);
    }
};

module.exports.removeTrainerById = [ async(req, res, next) => {
    try {
        await Trainer.removeById(req.params.id);
        
        res.send({_id: req.params.id});
    } catch (err) {
        next(err);
    }
},
notFound
];