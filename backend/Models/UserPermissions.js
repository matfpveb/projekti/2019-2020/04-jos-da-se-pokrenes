const permissionCheckers = require('./Permissions');

// We want to associate each permission (action) with a function that checks if an action is allowed
// TODO: Add stuff such as update-training, create-training, view/browse-profiles and so on
const Roles = {
    trainerRole: {
        role: 'Trainer',
        permissions: {'all': permissionCheckers.allPermitted,
                      'update-training': permissionCheckers.canUpdateTraining,
                      'view-profile': permissionCheckers.canViewProfile,
                     }
    },
    traineeRole: {
        role: 'Trainee',
        permissions: {'all' : permissionCheckers.allPermitted, // Returns true always
                      'view-profile': permissionCheckers.canViewProfile
                     } 
    }
};

const getRolePermissions = (role) => {
    if(role === 'Trainer'){
        return Roles.trainerRole.permissions;
    }
    else if (role === 'Trainee'){
        return Roles.traineeRole.permissions;
    }
}

const hasPermissions = function(permission, arguments){
    
    const permissions = getRolePermissions(arguments.user.kind);

    if(permission in permissions){
        return permissions[permission](arguments);
    }
    else{
        return false;
    }
};

module.exports.hasPermissions = hasPermissions;
