const mongoose = require("mongoose");
const options = { discriminatorKey: "kind" };

const Status = require("./StatusModel");

const { hasPermissions } = require('./UserPermissions');

const userScheme = new mongoose.Schema(
  {
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      auto: true,
    },
    username: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    credits: {
      type: Number,
      required: true,
      default: 1000,
    },
    imgPath: String,
    birthday: {
      type: Date,
      required: true,
    },
    gender: {
      type: String
    },
    followers: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      default: [],
      required: true,
    }],
    subscribedTo: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      default: [],
      required: true,
    }],
    statuses: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Status",
      default: [],
      requied: true,
    }]
  },
  options
);

userScheme.statics.getByUsername = async function (uname) {
  const user = await this.findOne({ username: uname }, { __v: 0 })
    .populate("statuses", { __v: 0 })
    .exec();

  if (user) {
    return user;
  } else {
    return undefined;
    // return Promise.reject();
  }
};

userScheme.statics.getById = async function (id) {

  const user = await this.findOne({ _id: id }, { __v: 0 })
    .populate("statuses", { __v: 0 })
    .exec();

  if (user) {
    return user;
  } else {
    return Promise.reject();
  }
};

userScheme.statics.getAll = async function () {
  return await this.find({}, { __v: 0 })
    .exec();
};

userScheme.statics.addStatus = async function (userId, status) {
  const user = await this.findById(userId).exec();

  if (!user) {
    return Promise.reject();
  }

  const addedStatus = new Status(status);
  await addedStatus.save();

  user.statuses.push(addedStatus);
  return await user.save();
};

userScheme.statics.getAllStatuses = async function (id) {
  const user = await this.findById(id).populate("statuses", { __v: 0 }).exec();

  const statuses = user.statuses;

  if (statuses) {
    return statuses;
  } else {
    Promise.reject();
  }
};

userScheme.statics.getStatus = async function (statusId) {
  const status = await Status.getById(statusId);

  if (status) {
    return status;
  } else {
    Promise.reject();
  }
};

userScheme.statics.removeStatus = async function (userId, statusId) {
  const status = await this.getStatusById(userId, statusId).exec();

  if (status) {
    await this.update({ _id: userId }, { $pull: { statuses: statusId } }).exec();
  } else {
    Promise.reject();
  }
};

userScheme.statics.getAllFollowers = async function (id) {
  const user = await this.findById(id).populate("followers", { __v: 0 }).exec();
  const followers = await user.followers;

  return (followers ? followers : Promise.reject());
};

userScheme.statics.getAllSubscribers = async function (id) {
  const user = await this.findById(id).populate("subscribedTo", { __v: 0 }).exec();
  const subscribers = await user.subscribedTo;

  return (subscribers ? subscribers : Promise.reject());
};

userScheme.statics.addSubscriber = async function (id, subscriberId) {
  const user = await this.findById(id).populate("subscribedTo", { __v: 0 }).exec();
  const subscriber = await this.findById(subscriberId).populate("followers", { __v: 0 }).exec();

  if (!user || !subscriber) { Promise.reject(); }

  await this.update({ _id: id }, { $push: { subscribedTo: subscriberId } }).exec();
  await this.update({ _id: subscriberId }, { $push: { followers: id } }).exec();
};

userScheme.statics.deleteSubscriber = async function (id, subscriberId) {
  const user = await this.findById(id).populate("subscribedTo", { __v: 0 }).exec();
  const subscriber = await this.findById(subscriberId).populate("followers", { __v: 0 }).exec();

  if (!user || !subscriber) { Promise.reject(); }

  await this.update({ _id: id }, { $pull: { subscribedTo: subscriberId } }).exec();
  await this.update({ _id: subscriberId }, { $pull: { followers: id } }).exec();
};

userScheme.statics.updateById = async function (id, user) {
  const curerntUser = this.getById(id);

  if (curerntUser) {
      const updatedUser = await this.updateOne({ _id: id }, { $set: user }).exec();
      return updatedUser;
  } else {
      return Promise.reject();
  }
};

userScheme.statics.hasPermissions = hasPermissions;

const userModel = mongoose.model("User", userScheme);

module.exports = userModel;
