const mongoose = require("mongoose");

const Address = require("../Models/AddressModel");

const gymScheme = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    name: {
        type: String,
        required: true,
    },
    address: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Address",
        required: true,
    },
    phone: {
        type: String,
        requred: true,
    },
    startTime: {
        type: Date,
        required: true,
    },
    endTime: {
        type: Date,
        required: true,
    },
});

gymScheme.statics.getAll = async function () {
    return await this.find({}, { __v: 0 })
        .populate("address", { __v: 0 })
        .exec();
};

gymScheme.statics.getById = async function (id) {
    const gym = await this.findById({ _id: id }, { __v: 0 }).populate("address", { __v: 0 }).exec();

    if(gym) {
        return gym;
    } else {
        return Promise.reject();
    }
};

gymScheme.statics.add = async function (gym) {
    const newGym = new this({
        name: gym.name,
        phone: gym.phone,
        startTime: gym.startTime,
        endTime: gym.startTime,
    });

    var address;

    if (!gym.address._id) {
        address = new Address(gym.address);
        await address.save();
    } else {
        address = await Address.getById(gym.address._id);
    }
    newGym.address = address;
    console.log(address);

    const addedGym = await newGym.save();
    return addedGym;
};

gymScheme.statics.updateById = async function (id, gym) {
    if (gym.address._id) {
        await Address.updateById(gym.address._id, gym.address);
        const updatedGym = await this.updateOne(
            { _id: id },
            { $set: gym }
        ).exec();
        return updatedGym;
    } else {
        const addressIdForRemove = (await this.getById(id)).address._id;
        var address = await Address.add(gym.address);
        gym.address = address;
        const updatedGym = await this.updateOne(
            { _id: id },
            { $set: gym }
        ).exec();
        await Address.removeById(addressIdForRemove);
        return updatedGym;
    }
};

gymScheme.statics.removeById = async function (id) {
    const gym = await this.getById(id);

    if (gym) {
        await this.deleteOne({ _id: id });
    } else {
        return Promise.reject();
    }
};

gymScheme.statics.getAllNames = async function () {
    const gyms = await this.find()
        .distinct('name')
        .exec();
    return gyms;
};

const gymModel = mongoose.model("Gym", gymScheme);

module.exports = gymModel;
