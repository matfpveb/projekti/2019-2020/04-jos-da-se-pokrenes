const mongoose = require("mongoose");

const User = require('./UserModel');
const Message = require('./MessageModel');

const chatRoomScheme = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    name: {
        type: String,
        required: true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    }],
    messages: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "ChatRoom",
        required: true
    }]
});

chatRoomScheme.statics.getChatRoomById = async function(id){
    const chatRoom = await this.findById(id, {_v: 0})
        .populate({
            path: 'messages',
            model: Message
        })
        .populate({
            path: 'users',
            model: User,
            select: 'id username kind'
        })
        .exec(); 
    
    return chatRoom ? chatRoom : Promise.reject();
};


chatRoomScheme.statics.getChatRoomHistoryById = async function(id){
    const chatRoom = await this.findById(id, {_v: 0})
        .populate({
            path: 'messages', 
            model: Message,
            populate: [{
                path: 'user',
                model: User,
                select: 'id username kind'
                // Match can be used here
            }]
        })
        .populate({
            path: 'users',
            model: User,
            select: 'id username kind'
        })
        .exec();
    
    return chatRoom ? chatRoom : Promise.reject();
};

chatRoomScheme.statics.addMessage = async function(message){    
    const chatRoom = await this.getChatRoomById(message.chatRoom);
    
    if(!chatRoom){
        return Promise.reject();
    }

    const addedMessage = new Message(message);
    await addedMessage.save();
    
    chatRoom.messages.push(addedMessage);
    return await chatRoom.save();
};

chatRoomScheme.statics.getMessages = async function(chatRoomId){
    const messages = await Message.find({chatRoom : chatRoomId})
        .populate('user')
        .exec();
        console.log(chatRoomId);
    
    return messages ? messages : Promise.reject();
};

chatRoomScheme.statics.getUsers = async function(chatRoomId){
    const chatRoom = await this.findById(chatRoomId, 'users')
        .populate({
            path: 'users',
            model: User,
            select: 'id username kind firstName lastName'
        })
        .exec();

    return chatRoom ? chatRoom.users : Promise.reject();
};

chatRoomScheme.statics.getChatRoomHeader = async function(chatRoomId){
    
    const header = await this.findById(chatRoomId)
        .populate({
            path: 'messages',
            model: Message,
            perDocumentLimit: 1, // Last message in each chat room
            select: 'message user time',
            populate : {
                path: 'user',
                model: User,
                select: 'id username'
            }
        })
        .select({"id": 1, 'name': 1, 'messages' : {$slice: -1}})
        .exec();
    
    return header ? header : Promise.reject();
};

chatRoomScheme.statics.getChatRoomHeaders = async function(userId){
    
    const headers = await this.find({
            users: {$in : [userId]}
        })
        .populate({
            path: 'messages',
            model: Message,
            perDocumentLimit: 1, // Last message in each chat room
            select: 'message user time',
            populate : {
                path: 'user',
                model: User,
                select: 'id username'
            }
        })
        .select({"id": 1, 'name': 1, 'messages' : {$slice: -1}})
        .exec();
    
    return headers ? headers : Promise.reject();
};

chatRoomScheme.statics.createChatRoom = async function(name, createdBy, users){
    try{
        const usersCopy = users.slice();
        
        usersCopy.push(createdBy);
    
        const chatRoom = new this({
            name: name,
            createdBy: createdBy,
            users: usersCopy
        });
        await chatRoom.save();

        return chatRoom;
    }
    catch(err){
        console.log(err);
    }
};

chatRoomScheme.statics.addUsers = async function(chatRoomId, users){
    const chatRoom = await this.findById(chatRoomId)
        .populate('users')
        .exec();
    
    if(!chatRoom){
        return Promise.reject();
    }
    
    chatRoom.users = chatRoom.users.concat(users);
    return await chatRoom.save();
};

chatRoomScheme.statics.removeUsers = async function(chatRoomId, usersIds){
    const chatRoom = await this.findById(chatRoomId)
        .exec();
    
    if(!chatRoom){
        return Promise.reject();
    }

    chatRoom.users = chatRoom.users.map(e => e.toString()).filter((e) => {
        return !usersIds.includes(e)
    });

    return await chatRoom.save();
};

chatRoomScheme.statics.getChatRoomWithUsersIn = async function(userIds){
    const chatRoom = await this.findOne({users : userIds})
    .populate({
        path: 'messages',
        model: Message,
        perDocumentLimit: 1, // Last message in each chat room
        select: 'message user time',
        populate : {
            path: 'user',
            model: User,
            select: 'id username'
        }
    })
    .select('id name messages')
    .exec();
    
    return chatRoom;
}


chatRoomScheme.statics.getPrivateChatRoom = async function(userIds){
    const user_1 = await User.findById(userIds[0])
    console.log('aa')
    const user_2 = await User.findById(userIds[1])

    const name_1 = user_1.username + ' ' + user_2.username;
    const name_2 = user_2.username + ' ' + user_1.username;
    
    
    const chatRoom = await this.findOne({users : userIds,
        name : { $in : [name_1, name_2] } })
    .populate({
        path: 'messages',
        model: Message,
        perDocumentLimit: 1, // Last message in each chat room
        select: 'message user time',
        populate : {
            path: 'user',
            model: User,
            select: 'id username'
        },
    })
    .select('id name messages')
    .exec();

    return chatRoom;
}


// and so on :)

const chatRoomModel = mongoose.model("ChatRoom", chatRoomScheme);

module.exports = chatRoomModel;
