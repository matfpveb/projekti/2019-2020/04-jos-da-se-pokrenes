const mongoose = require("mongoose");

const addressScheme = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    streetName: {
        type: String,
        required: true,
    },
    streetNumber: {
        type: Number,
        requried: true,
    },
    city: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    zip: {
        type: Number,
        required: true,
        //length: 5
    }
});

addressScheme.statics.getAll = async function () {
    return await this.find({}, { __v: 0 }).exec();
};

addressScheme.statics.getById = async function (id) {
    const address = await this.findById({ _id: id }, { __v: 0 }).exec();

    if (address) {
        return address;
    } else {
        return Promise.reject();
    }
};

addressScheme.statics.add = async function (address) {
    const newAddress = new this({
        streetName: address.streetName,
        streetNumber: address.streetNumber,
        city: address.city,
        country: address.country,
        zip: address.zip,
    });
    await newAddress.save();
    return newAddress;
};

addressScheme.statics.updateById = async function (id, address) {
    const currentAddress = this.getById(id);

    if (currentAddress) {
        const updatedAddress = await this.updateOne({ _id: id },{ $set: address }).exec();
        return updatedAddress;
    } else {
        return Promise.reject();
    }
};

addressScheme.statics.removeById = async function (id) {
    const address = await this.getById(id);

    if (address) {
        await this.deleteOne({ _id: id });
    } else {
        return Promise.reject();
    }
};

const addressModel = mongoose.model("Address", addressScheme);

module.exports = addressModel;
