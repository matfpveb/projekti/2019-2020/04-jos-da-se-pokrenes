const mongoose = require("mongoose");

const Trainer = require("./TrainerModel");

const trainingTypes = ["Weight lifting", "Stretching", "Pilates", "Yoga", "Calisthenics", "Cardio", "Aerobic", "Crossfit", "HIIT", "Flexibility", "Bodycombat", "Bodypump", "Body form", "Zumba", "Body fit"];

const trainingScheme = new mongoose.Schema({
    _id:{
     type: mongoose.Schema.Types.ObjectId,
     auto: true
    },
    name:{
        type: String,
        required: true,
    },  
    gym: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Gym",
        required: true,
    },
    startTime: {
        type: Date,
        required: true,
    },
    endTime: {
        type: Date,
        required: true,
    },
    
    maxSpots: {
        type: Number,
        required: true,
    },
    currentSpots: {
        type: Number,
        required: true,
    },
    difficulty: {
        type: Number,
        enum: [1,2,3,4,5],
        required: true,
    },
    trainingType: {
        type: String,
        enum: trainingTypes,
        required: true,
    },
    description:{
        type: String,
        required: true,
        default: "Ovaj trening nema dodatan opis.",
    },
    price: {
        type: Number,
        required: true,
    },
    trainer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Trainer",
        required: true,
    },
    traineesApplied: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Trainee",
            required: true,
            default: []
        }
    ]
});

trainingScheme.statics.getAll = async function () {
    //TODO: Populate
    return await this.find({}, { __v: 0 }).exec();
};

trainingScheme.statics.getById = async function (id) {
    //TODO: Populate
    const training = await this.findById(id, { __v: 0 }).exec();

    if (!training) {
        return Promise.reject();
    } else {
        return training;
    }
};

trainingScheme.statics.checkAndUpdate = async function (training) {
    const id = await this.findOne({ _id: training._id })
        .select("_id")
        .lean()
        .exec();

    if (!id) {
        return Promise.reject();
    } else {
        await this.updateOne({ _id: training._id }, { $set: training }).exec();

        return await this.getById(id);
    }
};

trainingScheme.statics.add = async function (training) {
    const addedTraining = new this(training);

    await addedTraining.save();

    const retrivedTraining = await this.getById(addedTraining._id);

    if (!retrivedTraining) {
        return Promise.reject();
    } else {
        return retrivedTraining;
    }
};

trainingScheme.statics.removeById = async function (id) {
    const training = await this.findById(id).populate("traineesApplied");

    if(training) {
        const trainees = training.traineesApplied;

        const trainer = training.trainer;

        if(trainer){
            await Trainer.update({_id: trainer._id}, {$pull: {trainings: id}}).exec();
        }

        await this.deleteOne({ _id: id }).exec();
    } else {
        return Promise.reject();
    }
};

trainingScheme.statics.getAllTraineesByTrainingId = async function (id) {
    const training = await this.findById(id).populate('traineesApplied').exec();

    const trainees = training.traineesApplied;
    if(trainees) {
        return trainees;
    } else {
        Promise.reject();
    }
};

trainingScheme.statics.getAllTypes = async function () {
    return trainingTypes;
};

const trainingModel = mongoose.model("Training", trainingScheme);

module.exports = trainingModel;
