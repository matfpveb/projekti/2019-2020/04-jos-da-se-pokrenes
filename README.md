# Project 04-Još-da-se-pokreneš

Društvena mreža za trenere i članove koji vežbaju u teretanama gde treneri mogu da objavljuju gde i kada drže treninge, da komuniciraju sa 
ostalim trenerima i članovima koji su prijavljeni na njihove treninge, objavljuju posebne ponude za svoje treninge. Ostali korisnici mogu da 
se informišu za svaku teretanu ko i kada drži koji trening i isto tako i za trenere pojedinačne, da se prijavljuju za trening u zavisnosti od 
slobodnih mesta, kao i da planiraju svoje treninge i komuniciraju sa trenerima. I jos po nesto.

## Pokretanje aplikacije
1. U database folderu se nalazi uputstvo za pokretanje baze: importDatabase.sh 
2. Locirati se na backend/Application, prvo pokrenuti **npm install**. Nakon intalacije pokrenuti **npm start**.
3. U odvojenom terminalu locirati se na frontend, prvo pokrenuti **npm install**. Nakon instalacije pokrenuti **ng serve**.


## Developers

- [Tamara Ivanović, 462/2018](https://gitlab.com/tasjenka7)
- [Katarina Bakić, 486/2018](https://gitlab.com/katarina_bakic)
- [Rajko Jegdić, 484/2018](https://gitlab.com/RajkoJegdicMatf)
- [Nikola Kovačević, 1104/2019](https://gitlab.com/one-orange-two)
- [Ivan Vranković, 489/2018](https://gitlab.com/ivan-vrankovic)
